---
title: "Bataille Navale"
date: 2020-10-29T08:00:00+01:00
tags: [Jeux, 2 Joueur-seuses]
draft: false
---

**Source** : [@Plumf](https://plumf.eu)  
**License** : [![](https://i.creativecommons.org/l/by-sa/4.0/88x31.png) Creative Commons Attribution -  Partage dans les Mêmes Conditions 4.0 International](http://creativecommons.org/licenses/by-sa/4.0/deed.fr)  
**Export** : 

---

## Déroulement d'une partie

Au début du jeu, chaque joueur place à sa guise tous les bateaux sur sa grille de façon stratégique. Le but étant de compliquer au maximum la tache de son adversaire, c’est-à-dire détruire tous vos navires. Bien entendu, le joueur ne voit pas la grille de son adversaire.
Une fois tous les bateaux en jeu, la partie peut commencer.. Un à un, les joueurs se tire dessus pour détruire les navires ennemis.

Exemple le joueur dit a voit haute H7 correspondant à la case au croisement de la lettre H et du numéro 7 sur les côtés des grilles.

Si un joueur tire sur un navire ennemi, l’adversaire doit le signaler en disant « touché ». Il peut pas jouer deux fois de suite et doit attendre le tour de l’autre joueur.
Si le joueur ne touche pas de navire, l’adversaire le signale en disant « raté » .
Si le navire est entièrement touché l’adversaire doit dire « touché coulé ».

Les pions blancs et des pions rouges servent à se souvenir des tirs ratés (blancs) et les tirs touchés (rouges). Il est indispensable de les utiliser pour ne pas tirer deux fois au même endroit et donc ne pas perdre de temps inutilement. Ces pions se placent sur la grille du dessus.

## Fin de la partie

Une partie de bataille navale se termine lorsque l’un des joueurs n’a plus de navires.

## Documents annexes

1. [Impression A4 pour 2 joueurs·euses](/bataille-navale-A4.pdf)  
**Source** : [fr.wikipedia.org - Grille de jeu imprimée](https://fr.wikipedia.org/wiki/Bataille_navale_%28jeu%29#/media/Fichier:Battleships_Paper_Game.svg)  
**License** : [![](https://i.creativecommons.org/l/by-sa/3.0/88x31.png) Creative Commons Attribution -  Partage dans les Mêmes Conditions 3.0 International](http://creativecommons.org/licenses/by-sa/3.0/deed.fr)  
**Export** : 
2. [Grille pour un·e joueur·euse](../bataille-navale.png)  
**Source** : [fr.wikipedia.org - Grille de jeu imprimée](https://fr.wikipedia.org/wiki/Bataille_navale_%28jeu%29#/media/Fichier:Battleships_Paper_Game.svg)  
**License** : [![](https://i.creativecommons.org/l/by-sa/3.0/88x31.png) Creative Commons Attribution -  Partage dans les Mêmes Conditions 3.0 International](http://creativecommons.org/licenses/by-sa/3.0/deed.fr)  
**Export** : 