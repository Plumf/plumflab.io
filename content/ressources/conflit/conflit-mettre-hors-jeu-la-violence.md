---
title: "Conflit mettre hors-jeu la violence"
date: 2020-10-17T08:00:00+01:00
tags: [Conflit, Gestion]
---

**Source** : Non-Violence Actualité  
**License** : © - Copyright  
**Export** : 

---

> Le conflit
> fait partie de la vie...
>
>Toute divergence de vues, de valeurs ou d'opinions peut se transformer en rapport de forces et engendrer un conflit. Le conflit est donc le résultat d’une confrontation de volontés opposées entre deux ou plusieurs parties, personnes ou groupes.
>
> Malgré l’image souvent négative que nous en avons, le conflit n’est ni bon ni mauvais. Selon la manière dont on va l’aborder et tenter de le résoudre, son issue sera destructrice ou constructive pour les personnes impliquées.
>
> Le conflit peut en effet avoir des fonctions et des conséquences positives telles que permettre la construction de relations plus justes, réaffirmer la règle commune dans le groupe, être source de développement pour les personnes...

## Prévenir la violence par une éducation à la résolution non-violente des conflits.

Dans nombre de conflits, les peurs et les ressentiments empêchent d'entendre les véritables revendications des protagonistes. La parole, quand elle ne juge pas, permet d'exprimer et de canaliser les émotions. Sinon, le conflit se transforme vite en conflit de personnes, en confrontation violente entre des adversaires qui n’ont plus qu’un seul objectif : vaincre l’autre à tout prix.

La fonction de la loi est d’assurer la sécurité et la justice pour tous, de sanctionner les délits. Mais tout conflit mal géré ne débouche pas, fort heureusement, sur une infraction pénale.

C’est donc à l'individu, au citoyen et à la collectivité à laquelle il appartient, d’assurer la prévention de la violence et la gestion constructive des conflits. Tel doit être l’un des principaux objets de l'éducation à la citoyenneté. La résolution des conflits, en effet, relève non seulement de l’éducation à la paix mais aussi de l’éducation à la vie démocratique.

Développer une approche non-violente du conflit nécessite des capacités qui pourraient être acquises dès l'école, à la fois par un enseignement spécifique et surtout par l'intégration de cette démarche au cœur même du projet pédagogique comme dans les structures de l'institution scolaire.

Apprendre à écouter, à exprimer ses sentiments, à négocier, à être médiateur. telles sont quelques-unes des orientations qui pourraient modifier sensiblement les rapports humains et sociaux. Les premières expériences de médiation scolaire sont, à ce titre, significatives.

Au-delà de l'école, tous les lieux d'éducation et de formation devraient ajouter à leurs enseignements et apprentissages d'utiles compléments sur la résolution non-violente des conflits. Il en est de même dans beaucoup d’autres domaines tels que l’entreprise, les administrations, les relations internationales...

## Les attitudes face au conflit

De l'accrochage avec ma voisine de palier au différend international, le conflit semble occuper tous les espaces. Un désaccord ne conduit heureusement pas toujours à une guerre, mais les deux situations ont en commun de pouvoir conduire à de graves déchaînements de violences. Les relations humaines comme les relations internationales sont en permanence secouées par des conflits, de toutes natures et de toutes gravités.
Confrontations de besoins, d'intérêts, de valeurs. il y a mille et une raisons d'entrer en conflit. Comment y réagit-on ? Cela va dépendre de l'image du conflit que nous avons assimilée, des modèles qui nous entourent et de notre propre expérience. Schématiquement, on peut définir quatre grandes caté- gories de réactions :
 - la négation
 - la démission
 - la confrontation violente
 - l'approche non-violente.

### La négation du conflit

 La réaction la plus courante face au conflit est la négation du problème. Mais, nier le conflit, c'est s’exposer à ce qu'il ressurgisse plus loin, plus tard et plus durement.

### La démission devant le conflit

l'objet du conflit. Cette attitude est souvent liée à la peur d'affirmer son opinion, ses droits, son pouvoir. La pseudo-équité consis- te à s'en tenir au strict moyen terme entre deux positions. Personne n'obtient satisfaction, le résultat est inefficace et ne répond pas aux intérêts des protagonistes.
On classe généralement la fuite au rang des attitudes de démission. Cependant, c'est par- fois la seule réponse efficace pour mettre la violence hors-jeu et laisser le temps aux tensions de s'apaiser avant d'envisager toute procédure de résolution du conflit.

![](/img/cmhjlv-01.png)

### La confrontation violente

Entrer sans délai dans la bagarre est, à coup sûr, une réaction énergique... même si elle n'est pas suffisamment réfléchie et organisée. On fonce “bille en tête”, on rend coup pour coup. Insultes, violences physiques et psychologiques... tous les moyens sont utilisés. La relation à l’autre va en faire les frais et le moindre incident va dégénérer rapidement en bagarre généralisée. Ce qui importe ce n'est pas la solution du conflit mais le maintien de son propre pouvoir. La confrontation violente bloque toute solu- tion ou conduit à de nouvelles situations chargées de conflits potentiels encore plus graves. La rancune et la rancœur de la "victime" se traduiront tôt ou tard par un désir de vengeance. La violence est donc non seulement inefficace pour trouver une solution au problème mais elle empêche l'émergence d'une nouvelle dynamique de résolution des conflits.

### L'approche non-violente

L'attitude non-violente face au conflit consiste à considérer la situation sans a priori, sans peur ni indifférence et à engager un processus de gestion qui puisse aboutir à une solution satisfaisante pour les parties en présence. Cette confrontation peut prendre diverses formes selon la nature du conflit.
 - Il peut s’agir d’une négociation qui permet d’oboutir à un compromis dans lequel chaque partie, après avoir affirmé clairement ses besoins et ses intérêts, cède une partie de ses prétentions.
 - Plus positive encore, la solution coopérative est élaborée conjointement par les protagonistes qui font preuve d’imagination et de respect de l’autre pour que les besoins et les intérêts de chacun soient réellement pris en compte. L'approche coopérative des conflits est une attitude de confrontation non-violente, d'opposition constructive. Cette attitude passe par la reconnaissance d'une image nouvelle du conflit qui restitue sa complexité et sa richesse.
 - Enfin, et si l’un des protagonistes n’est pas disposé à la négociation, la confrontation pourra nécessiter l’intervention d’une tierce partie médiatrice ou, plus radicalement, déboucher sur une lutte non-violente dont l'enjeu est d'exercer un rapport de force qui contraigne l'adversaire à la négociation.

|  Quelle solution ?                    |
|-------------------------------------------|
|De nombreux conflits de la vie de tous les jours proviennent de confrontations de besoins ou d'intérêts, de divergences de vues qui ne portent atteinte ni au respect de la personne ni aux droits de l'Homme. Le règlement de ces différends ne relève pas, le plus souvent, de l'institution judiciaire. |
| Quand les protagonistes considèrent le conflit comme un problème commun, il est possible de décrire une procédure de résolution en quatre étapes qui sert à la fois de grille d'analyse du conflit et de cadre pour agir.|
| 1. Définir un temps pour parler, La communication va permettre de faire état du différend, avec une volonté affichée de résoudre le problème. Il est nécessaire d'avoir du temps pour maîtriser ses émotions, casser l'engrenage de l'agressivité désordonnée et de la violence, et prendre le recul nécessaire. |
| 2. Exprimer clairement l’objet du conflit en s’efforcant de comprendre la logique de l’autre et de faire comprendre sa position (écoute active, affirmation de soi). Ce temps d'explications fait tomber la tension et permet le déroulement des étapes suivantes. |
| 3. Résumer le problème en identifiant ses causes. Ceci permet de voir si les deux parties ont la même appréciation de la situation. Cette étape est un pont entre la définition du problème et la recherche de solutions. Un médiateur peut parfois faciliter cette démarche. |
| 4. Faire preuve de créativité pour imaginer des solutions possibles et les examiner soigneusement en recherchant les avantages, les inconvénients, les conséquences pour le présent, pour l'avenir... Il faut que la solution retenue soit acceptable pour les deux parties, adaptée aux besoins, respectueuse de la relation. Prévoir éventuellement un suivi pour faire le point sur l'application de la solution retenue. |
| Il est parfois important de savoir reconnaïtre le désaccord même si aucune solution n'est trouvée dans l'immédiat, Il faut accepter qu'il n'y ait pas toujours de solution, sans pour autant faire monter la tension. | 

## Lexique 

### Non-violence

L‘acceptation du conflit et la confrontation de différences, demande de l'énergie, de la détermination, en un mot de & l'agressivité », définie comme la force vitale organisée de facon positive et constructive (on l'appelle aussi combativité). Au service de la vie, cette énergie ne recherche pas la négation ou la destruction de l'adversaire. Elle est tout entière tournée vers la mise à jour de l'objet du conflit et vers la recherche de solutions qui soient en accord avec les valeurs de respect de la vie, de droits de l'Homme et de démocratie. Par delà les différences - souvent à l’origine des conflits -, l’attitude non-violente consiste à préserver en permanence l’espace nécessaire au dialogue et à la négociation. S'en tenir à l'objet du conflit impose de dissocier la personne de son acte. Toute critique, comme toute sanction, doit viser un comportement et non la personne elle-même. L'approche non- violente des conflits, fondée sur le respect mutuel, n’exclut pas pour autant l'expression d’une grande fermeté et, si nécessaire, de rapports de forces quand il s’agit de défendre des droits bafoués ou de rétablir la justice.

### Médiation

La médiation est l'intervention d’une tierce personne pour 
établir - ou rétablir - la communication entre des personnes en conflit. Le médiateur n’a pas pour fonction de définir un gagnant et un perdant comme peut le faire le juge ou l'arbitre, mais de renouer les fils pour que les parties en présence retrouvent la maîtrise de « leur » conflit et tentent elles-mêmes d'y apporter une solution. 
La médiation s'exerce dans de nombreux domaines : des rapports interpersonnels aux relations internationales en passant par les problèmes de société. Depuis quelques années se développent des expériences de médiation à l’école, véritable apprentissage sur le terrain de la connaissance des conflits et de leur résolution.

## Changer notre approche du conflit

| Quelques idées recues... | et quelques suggestions |
|--------------------------|-------------------------|
| Le conflit est une forme de violence | Non, il y a souvent confusion entre les deux. Réveler les conflits qui peuvent se cacher derrière les violences permet de poser les problèmes, étape indispensable pour commencer à les résoudre. |
| S'il y a conflit, c'est qu'il y a échec de la relation | Non, le conflit fait partie de la relation. La résolution constructive du conflit est possible et souhaitable pour établir une relation plus juste. |
| La différence met en danger la relation | La relation, au contraire, a besoin d'espaces où chacun existe et communique avec l'autre, fort de ses différences et de ses ressemblances. La violence est précisément une tentative de réduire l'espace entre soi et l'autre. |
| Une communication franche suppose de dire à l'autre ce que l'on pense de lui et ce qu'il devrait faire | Non, ces attitudes favorisent le durcissement des conflits. L'affirmation de soi, l'écoute active, l'envoi de messages clairs. sont garants d'une communication efficace. |
| Pour en finir avec le conflit, il peut être utile de recourir à tous les moyens, même ceux de la violence | Construire une relation plus juste ne peut s'envisager que dans le respect réciproque, par le dialogue et la négociation. Il faut une cohérence entre la fin recherchée et les moyens utilisés. |
| Dans le conflit, il est parfois difficile d'éviter les procès d'intention et les invectives | Il est nécessaire de dissocier l'acte de la personne et s'en tenir strictement à l'objet du conflit pour éviter tout glissement vers un conflit de personnes. |
| Le conflit ne concerne que ses protagonistes |Etre spectateur peut parfois signifier être complice. Pour éviter l’engrenage vers la violence, il est utile de (r)établir un espace suffisant pour la relation et la communication. L'intervention d'un tiers médiateur peut y aider. |
| Le conflit est un combat avec un gagnant et un perdant | Les protagonistes doivent prendre conscience qu'ils ont un problème en commun. Îls doivent donc chercher à le résoudre ensemble. |
| Il faut s'accrocher à sa solution et la défendre jusqu'au bout pour ne pas « se faire avoir » | La solution au problème commun ne peut être que coopérative et construite à partir des besoins de l'un et de l'autre. Négocier permet d'aboutir à un compromis. |
| Le dialogue et la bonne volonté suffisent à réaler les conflits... | L'action non-violente est parfois nécessaire pour exercer un rapport de forces capable de contraindre l'une des parties à la négociation. |