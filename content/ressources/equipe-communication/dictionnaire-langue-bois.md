---
title: "Dictionnaire de la langue de bois"
date: 2020-10-25T08:00:00+01:00
tags: [Film, Court-Métrage]
draft: true
---

**Source** : [www.cahiers-pedagogiques.com - Les trois conceptions actuelles de l'autorité](http://www.cahiers-pedagogiques.com/Les-trois-conceptions-actuelles-de-l-autorite)  
**License** : © - Copyright  
**Export** : 

---

## Introduction

En 1968, un philosophe aujourd’hui oublié, Herbert Marcuse, nous mettait en garde : nous ne pourrions bientôt plus critiquer efficacement le capitalisme, parce que nous n’aurions bientôt plus de mots pour le désigner négativement. 30 ans plus tard, le capitalisme s’appelle développement, la domination s’appelle partenariat, l’exploitation s’appelle gestion des ressources humaines, et l’aliénation s’appelle projet. Des mots qui ne permettent plus de penser la réalité, mais simplement de nous y adapter en l’approuvant à l’infini. Des «concepts opérationnels» qui nous font désirer le nouvel esprit du capitalisme, même quand nous pensons naïvement le combattre. Notre langage est doucement fasciste, si l’on veut bien comprendre le fascisme comme l’élimination de la contradiction. Georges Orwell ne s’était pas trompé de date : nous avons failli avoir en 1984 un «ministères de l’intelligence». Assignés à la positivités, désormais, comme le prévoyait Guy Debord : Tout ce qui est bon apparaît, tout ce qui apparaît est bon....

Ainsi, par exemple, nous sommes tous plus ou moins conscient de l'impérieuse nécessité de nous opposer à la « démarche qualité », s'agissant de l'intervention sociale, culturelle, éducative, ou médico-sociale. Mais à moins d'avoir sérieusement approfondi la question, cette nécessité reste pour le moment au niveau d'une intuition. Il nous semble que quelque chose ne va pas dans cette démarche. Mais comment s’opposer à la « qualité » à moins de passer pour un fou ou un saboteur ? Nous ne le pouvons pas ! A moins de dévoiler le mensonge du langage, nous sommes désormais condamnés à accepter TOUT ce qui se présente sous cette démarche.

La question stratégique qui se pose à nous est donc : « comment nous réapproprier un langage critique » qu’on nous a interdit, volé, maquillé, dont on nous a dépossédé ? De quelle manière ? cela est-il simplement possible, et à quel prix ? Nous appelons « éducation populaire », ce travail de réappropriation, d’interrogation des évidences, et de reconquête d’une pensée critique.

## Acteur(s)

Au sein de nombreuses entreprises et administrations comme à l'Éducation Nationale, chacun est invité à être l'« acteur de son propre changement », - portant sur ses épaules le poids d’une responsabilité étrange et difficile à assumer. Telle association d’animation propose à ses adhérents, à travers la fréquentation des activités de loisirs, de « devenir acteur de leur propre vie »...! Étrange pléonasme 

Avec le triomphe de l’individualisme, et l’aide de quelques sociologues (comme Michel Crozier), la théorie du « jeu de l’acteur » propose de s’émanciper des déterminismes (de classe, de groupe, de travail, de famille, de communauté...) et de prendre conscience de sa propre marge de manœuvre. Cette proposition admirable qui en appelle au libre arbitre de chacun, n’a qu’un petit défaut : renvoyer le balancier un peu trop fort dans l’autre sens, à une période où la domination, pour se rendre discrète, aimerait que l’on en finisse avec la mobilisation collective, les luttes, les idées, les batailles politiques que l’on ne mène jamais seul.

A la différence d’un « agent », un « acteur » interprète et ne se contente pas d’obéir et d’appliquer. C’est pour cela qu’on parle d’un agent de police et d’un acteur de théâtre. Personne ne voudrait voir la police interpréter librement la loi, et on attend d’un policier qu’il ne se comporte surtout pas en acteur, mais bien en agent...Peut-on en dire autant des travailleurs militants de la jeunesse, de la culture, de l’éducation, du social ? Ne serait-on pas en droit d’attendre d’eux qu’ils se comportent en interprètes des politiques aberrantes dont ils sont aussi des agents ?

Pour désigner les agents, dans le discours des politiques publiques, il n’est question que des « acteurs ». Un Comité local de prévention de la délinquance veut réunir les « acteurs » locaux de la jeunesse, un plan local d’insertion, les « acteurs » de l’insertion, et un contrat éducatif local les « acteurs » de l’éducation. En réalité, il n’y a que des agents. Il n’y a pas l’ombre d’un comportement d’acteur de leur part, et le premier « agent d’insertion » qui se prendrait pour un acteur et qui s’essaierait à discuter du bien fondé ou des contradictions des dispositifs d’insertion se verrait vertement rappelé à l’ordre ! C’est ainsi que les animateurs n’ont rien le droit de dire sur les politiques d’insertion, ou que les parents n’ont rien le droit de dire sur l’enseignement.

Appeler les gens des « acteurs », c’est leur faire croire qu’ils ont une liberté quand ils n’en ont aucune. C’est les culpabiliser encore un peu plus (que les choses aillent si mal, c’est de leur faute puisque ce sont les acteurs) C’est faire en sorte que les gens se sentent individuellement responsables de la situation et qu’ils ne se posent plus jamais de questions politiques.

Quand il n’y aura plus de groupes, il n’y aura plus que des acteurs responsables de leur propre situation, à qui iront-ils se plaindre ? A leur image, réfléchie à l’infini dans le miroir de leur loge d’acteur.

### Exercice de traduction

Les acteurs de l'Éducation dans la Ville sont invités à se réunir pour définir ensemble le
projet éducatif du territoire.

Les parents otages, les élèves victimes, les enseignants dépressifs et les animateurs complices sont priés de faire semblant de désirer officiellement leur propre domination, sachant qu’on ne peut rien faire d’autre et que de toutes façons ce sera comme ça et pas autrement !

## Compétence(s)

Management / jugements psychologiques de la personne remplaçant « métier » et « qualification ». Permet de détruire la mobilisation collective au profit de  'individualisation des carrières. Directement importées des techniques du management libéral, les « compétences » ont avantageusement remplacé « la qualification » et « le métier » dans le langage des formateurs et des employeurs, (notamment associatifs). Le patronat veut se débarrasser des « métiers » qui permettent de résister collectivement et de s’organiser syndicalement. Comme il faudra désormais en changer cinq à six fois dans une vie, ce sont les « compétences » à s’adapter, à se rendre « employable » qui serviront désormais à évaluer les travailleurs (pardon – les « collaborateurs »). Finissons en avec le « savoir faire » et célébrons l’ère nouvelle du « savoir-être ». Savoir être docile, souple, interchangeable, malléable, motivé, imaginatif, créatif, convivial, communiquant... et plaisant à son employeur (pardon – à son collaborateur). C’est la personne, son être intime, sa vie privée, sa culture, son comportement, qui doivent être enrôlés dans la productivité du service... au nom de la qualité, ou du militantisme et de la noblesse des missions (culture pour tous, sport pour tous, loisirs pour tous...) et pour garder les subventions, c'est-à-dire la part de marché public.
On peut ainsi, à l’école et dans l’entreprise découper le comportement du travailleur en
sous-compétences , telles que « manifester l’envie d’apprendre », « accepter des activités
contraignantes », « savoir être autonome », « faire preuve d’initiatives », « gérer son
temps » ou « respecter les règles de vie au sein de l’association ». Autant de critères qui
relèvent de l’expérience personnelle et non d’une rationalisation des tâches. On n'y
trouvera curieusement aucune compétence telle que « tenir tête à un maire », « contester
son patron », « questionner le bien-fondé d'une décision » ou « résister à une dérive
marchande de l’association » !
Pourtant, le travail n’est pas simple affaire de compétences. Il s’y tisse aussi un univers de
coopération et de conflits qui dessinent une identité collective et professionnelle. Avec
l’évaluation de ses compétences, l’animateur socioculturel sent peser sur lui la pression à
être conforme, pour le bien politique et financier de l’association. Comment des
associations progressistes forgées dans une histoire des luttes sociales on pu en arriver à
relayer en toute naïveté et bonne conscience ce discours réactionnaire n’est pas un
mystère : c’est la grande victoire du capitalisme qui est d’abord – on ne le dira jamais
assez – une victoire sur les mots, n’en déplaisent à ceux qui croient encore que les mots
ne sont que des mots, et pas une manière d’agir !
Exercice de traduction
Le portefeuille de compétences des nouveaux emplois-jeunes médiateurs de quartier, sera
régulièrement évalué par leurs tuteurs avec les partenaires de l’action.
La souplesse, la soumission et la collaboration active et ardente des jeunes payés pour
calmer la rébellion de leurs copains, leur capacité à intégrer le discours des institutions
fera l’objet d’un contrôle avec les financeurs.
Page 5Culture
Pourquoi ce mot connaît il depuis trente ans un succès tel que même le sport éprouve le
besoin de justifier qu’il est aussi une pratique « culturelle » ?
Grâce à l’école, le capitalisme déguise son exploitation en faisant croire que chacun est
responsable de sa place dans l’échelle sociale, et des efforts qu’il a mis à se cultiver. Si
l’on est opérateur système à la chaîne, c’est par paresse intellectuelle, nous avions les
mêmes chances au départ ! Grâce à l’école, la culture est une machine à classer les gens
en les faisant se sentir coupables.
Depuis trente ans, la référence au « culturel » sert à effacer, détruire, et remplacer la
référence au « politique ». Par exemple, si l’excision est condamnable d’un point de vue
politique (qui consiste à préférer des valeurs telles que l’égalité de l’homme et de la
femme), on nous apprend qu’elle est éminemment respectable en tant que « pratique
culturelle ». On n’a rien à dire de la « culture » des autres, parce que la culture est sacrée.
C’est d’ailleurs à cela que sert cette nouvelle religion : que l’on ne puisse plus rien dire !
Avec la culture, tout se vaut
Pire encore : en ramenant discrètement la question de la culture à celle de l’artiste, les
socialistes au pouvoir des 1981 consacrent la figure managériale du travailleur nouveau :
créateur, producteur indépendant, autonome, ludique, inventif, non-revendicatif,
férocement individualiste... A la place du militant politique, collectif, poussif et besogneux
des années 70, le pouvoir encourage l’artiste, non pas celui qui voudrait délivrer un
message (quelle horreur), mais celui qui exprime un narcissisme chahuteur, adolescent,
provocateur et rigolo, celui qui exprime le vide, le rien, la dérision de toutes les idées et de
toutes les utopies, la moquerie du politique et des valeurs. On encourage l’imaginaire à
condition qu’il ne mène vers aucune vision politique. On ne doit plus croire en rien car
c’est ringard. On doit jouir de la crise et de la mondialisation qui est une chance pour
l’homme, enfin seul, débarrassé des pesants collectifs. Il n’y a plus d’emploi dans les
banlieues, mais il y a l’art et la création. Que les enfants des immigrés dansent du hip-hop,
cela convient au pouvoir : pendant qu’ils deviendront des « créateurs », ils ne feront pas
de syndicalisme.
Comme le disait Malraux en 1961 en inaugurant la première Maison de la Culture à
Bourges : « Nous allons enfin savoir ce qui peut être autre que le politique dans l’ordre de
l’esprit humain »....Eh bien voilà qui est fait ! En 1968, Francis Janson, quant à lui,
proposait d’appeler Culture ce qui permet de se choisir politiquement... mais il est vrai que
c’était en 1968 !
Page 6Citoyenneté
Habilement confondue avec la « civilité », la « Citoyenneté » détient le triste record du
concept le plus galvaudé, récupéré, mis à toutes les sauces, pour justifier la soumission à
la domination. On a aujourd’hui des entreprises « citoyennes » parce qu’elles consentent
magnanimement et contre exonération de cotisations sociales, à exploiter un CES et deux
RMA de plus ! On entend ainsi couramment proférée cette définition étrangement
judiciaire, (voire policière) : « le citoyen est celui qui a des droits et des devoirs » - (on
comprend surtout qu’il a des « devoirs ») - et l’on repense à cette phrase du philosophe
Jankélévitch : « Je serai toujours le gardien de tes droits, jamais le flic de tes devoirs ».
Quant à ses droits, il ne manquerait plus qu’il en revendique l’usage ! La citoyenneté est
fort habilement confondue aujourd’hui avec la civilité, voire le civisme. Serait alors citoyen
celui qui se tiendrait bien. Celui qui ferait du sport ou de la musique au lieu de brûler des
voitures, bref, celui qui accepterait sans broncher – et autant dire « sportivement » sa
condition de sans-avenir, de sans-emploi, de sans-espoir, de sans-argent... sa condition
d’exploité, de dominé, d’aliéné, sans faire d’histoires, sans se rebeller. Quelqu’un qui
jouerait le jeu, qui serait beau joueur et bon perdant. C’est ainsi par exemple que
l’éducation Nationale publie des « chartes de la citoyenneté à l’école » qui ne sont que des
listes interminables d’interdits et de sanctions, des règlements intérieurs raffinés sans
autre contrepartie que l’arbitraire des enseignants et le droit de se taire.
Rectifions les choses : « Est citoyen celui dont la volonté produit du droit » ! Qu’on lise et
relise cette définition à haute voix jusqu’à la comprendre. Une société de droit ne
progresse que par extension du droit, par la conquête, toujours, de nouveaux droits (pour
les femmes, pour les pauvres, pour les jeunes, pour les étrangers, pour les faibles...) vers
toujours plus d’égalité. Or « le droit n’advient pas par le moyen du droit ». Aucun patronat
n’a jamais de sa propre initiative, accordé de nouveaux congés payés. Tout nouveau droit
a toujours été arraché par la force, par le combat, souvent par l’illégalité (avortement). Il
est alors facile de comprendre que le citoyen n’est pas celui qui joue au hand-ball ou qui
accepte de faire du rap à la MJC. Celui-ci est sur la voie de la civilité (respect de l’ordre et
de la paix sociale) mais pas encore sur la voie de la citoyenneté, concept politique. On ne
connaît toujours qu’une seule voie d’accès à la citoyenneté : la participation au conflit
social. Amener un jeune à devenir citoyen, c'est-à-dire sujet politique, c’est l’amener à
participer au conflit social, à en comprendre le sens, à y prendre position. L’amener à taire
sa révolte, c’est le contraire d’un chemin vers la citoyenneté. Les associations d’animation
socioculturelle devraient y réfléchir à deux fois.
Exercice de traduction
Dans le cadre de la politique de la ville, le ministère de la culture encourage la création
citoyenne des jeunes.
Dans le cadre du contrôle social, l’administration des beaux-arts finance les petits rebelles
qui acceptent de manifester leur intégration à l’ordre établi par le ralliement aux simagrées
esthétiques de la société libérale.
Page 7Cohésion sociale
La cohésion sociale est un concept généralement employé par les responsables de la
droite modérée souhaitant donner un volet social à leur action politique sans souscrire à
l'idée de gauche de lutte contre les inégalités. Traditionnellement, l'approche qualifiée « de
droite » en France considère les inégalités comme naturelles. C'est pourquoi il est vain,
selon cette approche, de vouloir lutter contre elles. La pensée « de gauche » considère
que les inégalités sont le fruit de la société et fait de la lutte contre les inégalités l'un des
fondements de son action tandis que la droite cherche à donner suffisamment de liberté à
l'individu pour qu'il puisse se réaliser et, accessoirement, maintenir la cohésion de la
Nation en évitant que les inégalités devienne trop criantes.
Comme tout irait bien si les pauvres, en plus d’être exploités, refusés à l’emploi,
discriminés au logement, humiliés à l’école, etc... se tenaient sages et acceptaient
humblement leur condition sans faire de vagues ! Ah la belle cohésion sociale que l’on
aurait là : les riches au bord de leur piscine, les pauvres dans leur cage d’escalier, et tout
le monde respectant ce bel agencement naturel !
Ce concept qui sert à identifier les « frontières » de la collectivité, soit les inclus, les
marginaux et les exclus, repose toujours sur le partage de valeurs communes, d’un certain
art du « vivre ensemble ». Les « enthousiastes » de la cohésion sociale y voient un
moyen utile de masquer les effets de la violence économique par un appel au respect de
valeurs partagées. On ne brûle pas une voiture (c’est contre la cohésion sociale) mais on
peut licencier 7 000 pères de famille dans la même ville (Michelin) ou rafler la distribution
de l’eau dans un pays d’Afrique et faire crever quelques milliers de pauvres
supplémentaires en multipliant le prix de ce précieux liquide (Vivendi), cela ne nuit
nullement à la cohésion sociale. Les multinationales européennes qui ont fait une razzia
sur les entreprises des pays de l’est en propulsant le chômage à 40% sont attentives, par
la voix du conseil de l’Europe, à ce que ces gouvernements veillent à leur cohésion
sociale avant d’intégrer le paradis capitaliste européen .
L'État qui laisse les directions d’entreprises détruire délibérément des millions d’emplois
en pleurnichant sur l’aide la concurrence, semonce les travailleurs sociaux afin qu’ils
suscitent (par quel miracle ?) au sein du peuple :
– Un sentiment d’appartenance à une même communauté, de partager les valeurs
d'un même collectif ;
– Un sentiment de participation qui n’est en fait qu’une information sur les décisions
déjà prises ;
– Un sentiment de tolérance envers les autres pauvres et qui prêche l'acceptation du
multiculturalisme ;
– Un sentiment de respect des institutions publiques et privées supposées agir
comme médiateurs des conflits.
Quand les pauvres ne marchent pas, l'absence de réalité ou de consensus sur une ou
plusieurs de ces dimensions explique les états d'in-cohésion sociale d'une société ou
d'une communauté.
Exercice de traduction
Soucieux de mixité sociale, les acteurs de la politique de la Ville veilleront à favoriser la
cohésion sociale dans les opérations de renouvellement urbain.
Page 8Afin de disperser les pauvres sans qu’ils ne se défendent, les agents du contrôle social
veilleront au maintien de l’ordre pendant les opérations juteuses de démolition-
reconstruction.
Page 9Contrat
Contrairement à la tradition révolutionnaire française pour laquelle l'individu est titulaire de
DROITS inaliénables dès sa naissance ou conquis par la lutte, dans la philosophie libérale
anglo-saxonne les relations sociales sont supposées régies par une sorte de « contrat »
volontaire dans lequel l'homme accepte une certaine aliénation en échange d'une certaine
sécurité. La différence n'est pas mince. Ce pseudo « contrat » est une façon de faire
accepter par la victime sa domination, son oppression ou son aliénation, en faisant croire
à son adhésion libre et volontaire à cette agression. Ce « contrat » sert à éteindre sa
révolte.
N'importe quel éducateur a usé jusqu’à la corde le vieux gag qui consiste à « passer un
contrat avec les jeunes »... et le « contrat d'établissement » que l'école fait signer
aujourd’hui aux élèves sous prétexte de citoyenneté n'est qu'une liste d'interdictions, un
marché de dupes !
Comme le déclarait tel maire lors d'un colloque sur la banlieue : « j'ai passé un deal avec
les jeunes... une promesse de pistes de skateboard contre une baisse sensible des
incivilités pendant un an. ».
L'expression « acheter de la paix sociale » prend ici tout son sens ! Il y a encore beaucoup
de travail pour expliquer aux élus qu'ils ne sont pas là pour« dealer », mais pour donner à
la souffrance sociale des jeunes une traduction politique dans l'espace public.
En droit français, un contrat suppose la liberté des contractants et une égalité de position.
Il n'y a pas de « contrat » entre les ASSEDIC et un chômeur qui est obligé de signer le
PARE pour avoir le droit de manger.
L'indemnisation de perte d'emploi n'est pas le fruit d'un « contrat » mais d’un DROIT,
toujours à préserver et à défendre. En prenant les ASSEDIC au mot, la justice de Marseille
qui rétablit les chômeurs dans leur DROIT rappelle que le contrat, en France, reste un
dispositif juridique contraignant, même quand on veut l'utiliser comme une entourloupe
idéologique à l'américaine. Aucun chômeur sain d'esprit ne signerait de sa propre volonté
un engagement à une « recherche positive d'emploi » telles que le PARE l’y oblige
actuellement. Ce « contrat » honteux fait peser sur le chômeur la responsabilité de sa
situation et fait oublier que si le chômeur en est là, c'est qu'un patron l’a licencié contre
son gré.
« Contrat Emploi Solidarité », contrat d’emploi « consolidé », combien d’entorses au droit
du travail nous réserve-t-on encore pour nous exploiter à moindre coût ? Grâce aux
« contrats » il n'y a plus de patrons ni d'employés, d’exploiteurs ni d’exploités, de mairies
donneuses d'ordre et d'associations exécutantes, il n'y a plus que des « partenaires »...
La vie est belle !
Quand le recours au contrat brise le contrat social, le « plaider coupable » n’est pas loin.
Dans cette autre histoire, Droit et Justice n’ont pas dit non plus leur dernier mot.
Exercice de traduction
Les élèves et les enseignants ont été invités à signer le contrat de citoyenneté du lycée.
Les élèves n’ont pas eu d’autre choix que de se soumettre au nouveau règlement
disciplinaire.
Page 10Développement
Développement local, développement social, développement culturel... Dans le quatrième
pays le plus riche du monde - la France - nos actions doivent dorénavant produire du
développement, terme jusque là réservé aux pays dits « sous-développés ».
L’envahissement de la pensée économique et de son vocabulaire correspond en France à
l’arrivée des socialistes au pouvoir. Dès la conférence de Mexico en 1981, (rompant avec
une tradition de l’éducation populaire faisant du « développement culturel » l’arme contre
le développement économique - Joffre Dumazedier), Jack Lang déclare que désormais,
développement culturel et développement économique ne sont qu’une seule et même
chose. Qu’on se le dise ! La pensée simpliste du capitalisme (plus c’est mieux donc mieux
c’est plus) impose à toute activité de produire de l’accumulation. Même sous-employés, il
faut plus d’équipements (culturels, sportifs... et plus de rond-points). Le développement
local est une mise en concurrence des territoires les uns contre les autres, et la fin des
solidarités nationales. Guerre des images et de la subvention. Séduction des entreprises,
audimat. Le développement social est la pacification des conflits pour positiver l’image de
la commune, le développement culturel est la conquête forcée des publics de la nouvelle
marchandise culturelle. Or la croissance ne produit pas de développement. Trente années
de croissance ininterrompue produisent 20 millions de chômeurs et 60 millions de
précaires dans l'Europe « développée » et accroissent partout l’inégalité. Confondre
développement et croissance, c’est ne pas comprendre que la condition du
développement est d’abord le conflit démocratique et non l’harmonie factice du
partenariat. Apprenons à nous débarrasser de ces termes pour réapprendre à penser par
nous-mêmes.
Exercice
Remplacez « développement » par « capitalisme » dans la phrase suivante :
Le développement local combine du développement social, du développement culturel
avec du développement économique pour produire du développement durable.
Page 11Éducation tout au long de la vie
Derrière cette généreuse formulation, l’attaque coordonnée du patronat européen : la
condamnation à se former jusqu’à ce que mort s’ensuive ! Pour se rendre « employable »
et faire disparaître la notion de « métier » (et toutes les mobilisations syndicales qui y
étaient liées) il faut remplacer les « qualifications » établies une fois pour toutes, par des
« compétences » changeantes. Vive l’entrée dans la « précarité » et « l’imprévisibilité »,
vive la « société du risque » chère au MEDEF qui n’en prend aucun. « La vie est précaire,
l’amour est précaire, pourquoi le travail devrait il ne pas l’être ? » (L. Parisot, Présidente
du MEDEF).
Le concept « d’éducation tout au long de la vie » inventé par Jacques Delors, a été
développé et diffusé par l’OCDE, boîte ultra libérale au service du patronat européen, très
éloignée des préoccupations gauchistes de promotion du peuple Aujourd’hui, l’éducation
et la formation tout au long de la vie absorbent la totalité de la formation et doivent à la fois
mettre l’accent sur l’apprentissage qui va de l’enseignement préscolaire jusqu’à l’après-
retraite, et couvrir toute forme d’éducation, qu’elle soit formelle, non formelle ou informelle.
Le plus horripilant n’est pas le cynisme du patronat, mais la crédulité avec laquelle les
associations socioculturelles ont adopté cette nouvelle gourmandise en y voyant la mise
en œuvre du projet d’éducation populaire, ou peut-être plus pragmatiquement un bon petit
marché juteux !
Les exigences des employeurs se sont imposées en douceur grâce à ce petit mensonge
de traduction : en Europe, les textes parlent bien d’apprentissage tout au long de la vie.
Déjà en 1971 une victoire de l’immédiat après 68, la loi sur « l’éducation permanente »
avait glissé vers des dispositifs de « formation continue » au service des entreprises, ce
qui n’est pas du tout la même chose. Même si la part d’utopie qui fait la force de l’idée
d’éducation permanente a suscité quelques mesures plus conformes à l’ambition d’origine
(comme le congé individuel de formation introduit dans la législation en 1982), le résultat
est que ce sont les actions courtes d’adaptation aux évolutions de l’emploi qui se sont
taillées la part du lion. Si vous êtes chômeurs, demandez donc à l’ANPE ou aux Assedic
de vous faire financer une licence d’histoire, je vous parie qu’on vous proposera un stage
court de découpage en poissonnerie
L'enseignement doit soutenir la compétition économique tout en se privatisant. Un
dualisme des niveaux d'instruction doit s'installer puisque 65% des créations d’emplois en
Europe dans les dix prochaines années seront des emplois non-qualifiés. A quoi bon
former des philosophes ? l'éducation est un formidable marché qui représente en volume
le double du marché automobile mondial, mille milliards de dollars scandaleusement laissé
aux service publics et qui échappent à la spéculation privée. Si l’union européenne veut
être « l’économie de la connaissance la plus compétitive », il faut démanteler les systèmes
nationaux d’éducation, c’est sa priorité sous pression des grands groupes privés éducatifs.
Il va falloir « apprendre à apprendre », c'est-à-dire apprendre chez soi, sur son temps de
loisir non pris en charge par l’employeur, avec des logiciels éducatifs ! Vive la « société de
la connaissance et de l’information », vivent les « NTIC ».
Exercice de traduction
Dans la société de la connaissance, les jeunes devenus adultes auront le droit de
continuer à s’éduquer tout au long de la vie.
Dans le capitalisme culturel tertiaire informatisé, ceux qui ne s’adaptent pas rapidement
aux innovations technologiques seront exclus de la société.
Page 12Égalité des chances
Après avoir éliminé Robespierre, la réaction thermidorienne de 1795 met fin à la révolution
et à l’idéal d’une société dans laquelle l'égalité serait réalisée. Mais comment consacrer le
retour de l’inégalité, de l’argent, de l’aristocratie, de la fortune, de la propriété sans que le
peuple ne reprenne les armes ? Comment abandonner l'égalité sans que cela ne se voie ?
En l’appelant : « l’égalité des chances ». L’égalité des chances est le mot qui veut dire
« inégalités ». Tels le lapin et la tortue, nous sommes donc « égaux » sur la ligne de
départ. Nous avons virtuellement les mêmes « chances ».
En entrant à l’école, Bastien dont le père est banquier, et Mohammed, dont le père est
chômeur ont donc les mêmes « chances ». Il est évident que si Bastien intègre une
grande école et Mohammed ne dépasse pas la troisième professionnelle, ce n’est que le
résultat de leur mérite propre. L’un n’a pas su, ou pas voulu utiliser les « chances » que
l’on avait mises à sa disposition en égale proportion avec l’autre. Quand l’un et l’autre
doivent raconter leurs vacances dans une rédaction, c’est un pur hasard si les parents de
Bastien et le professeur possèdent la même culture, partagent les mêmes codes, les
mêmes modes de vie, et aiment tous les deux les mêmes vacances : marcher dans le
Cantal plutôt que de s’entasser au camping des flots bleus et faire du baby foot au café de
la plage. Ou encore rester jouer au foot tout l’été sur la dalle de l’immeuble. Si la rédaction
de Bastien (à qui sa maman a appris à reconnaître les chants d’oiseaux dans les forêt du
Cantal) reçoit une meilleure note, c’est parce que Mohammed n’a pas voulu faire l’effort de
raconter dans des termes joliment et littérairement tournés ses journées d’été occupées à
traîner dans son hall d’escalier. Un tel sabotage de ses « chances », une telle mauvaise
volonté, une telle paresse intellectuelle méritent une sanction. Un tel refus d’utiliser les
« chances » que l’école républicaine a mises à sa disposition mérite une mauvaise note.
Comme tous ces mots à dépolitiser les rapports sociaux, « l’égalité des chances » est une
machine à nous faire croire que cette société offre à tous une égale opportunité et que
nous sommes seuls responsables de notre situation. C’est le modèle Américain du « land
of opportunity ». Il n’y a plus de patrons pour nous exploiter, seulement des individus qui
ont voulu ou pas saisir leurs chances.
Transformé en loi, ce principe de l’égalité des chances légitime l’abjection des « grandes
écoles » dans lesquelles se côtoie et se reproduit « l’élite républicaine » qui est
essentiellement le refuge de la noblesse et de l’aristocratie reconstituées. Depuis la loi sur
l’égalité des chances, un quota de pauvres et d’étrangers (pardon...de « minorités
visibles ») est autorisé à rentrer dans ces écoles. Mais la question n’a jamais été de savoir
combien y entraient, mais combien en sortaient. Au nom de l’égalité des chances, tout le
monde rentre dans l’entonnoir de l’école, mais seulement 1% de fils d’ouvriers en sortent
avec un diplôme universitaire quant la France dénombre 30% d’ouvriers en 2005 au sein
de la population active. Jetons un voile pudique sur les 99 autres... Le problème de
l’égalité n’est pas de rentrer mais de sortir égaux, pas de démarrer mais de finir égaux.
C’est une toute autre tâche !
Exercice de traduction
Pour aider les parents à exercer leur autorité parentale, la loi sur l’égalité des chances
institue un contrat de responsabilité parentale qui permet d'assigner des objectifs aux
parents. S'il n’est pas respecté par la famille, les autorités compétentes pourront
prononcer la suspension provisoire de certaines allocations. (texte officiel)
Pour punir les familles dont les enfants ne se soumettent pas à l’oppression et à la
pauvreté, la loi pourra pénaliser financièrement les parents pour leur apprendre à faire
Page 13respecter l’humilité de leur condition.
Page 14Engagement
Raffinement de l'anti langage, le concept d'engagement signifie aujourd'hui toute forme de
manifestation non-politique, voire anti-politique. Au nom de l'engagement, on cesse de
discuter ou de brasser des idées (politiques) pour agir « concrètement ». La forme par
excellence de ce nouvel « engagement » est l'action humanitaire. Triomphe de l’idéologie
de droite dans le langage, « l’engagement » signifie aujourd’hui toute forme de
participation non-politique, voire anti-politique. Le ministre Luc Ferry l’a dit :
« l’engagement des jeunes » sera l’axe fort de sa politique. On se frotte les yeux : va-t-on
se réveiller du long sommeil libéral-socialiste et revenir à notre mission d’éducation
politique des citoyens ? Que nenni : l’engagement en question est civil, associatif, sportif,
culturel, humanitaire... mais pas politique ! Il y a vingt ans, l’engagement signifiait l’acte de
se choisir politiquement. Les travailleurs sociaux, les artistes, les jeunes, qui ne
« s’engageaient » pas étaient regardés avec pitié. Aujourd’hui c’est l’inverse !
Curieusement, ceux-là même qui font carrière (locale, régionale, nationale) via les partis,
considèrent comme un péché que leurs concitoyens s’intéressent à la politique.
Le nouvel engagement doit être concret, ciblé, et pragmatique. Les idées abstraites
conduisent au goulag ou au terrorisme. Se méfier des idées, se méfier de la réflexion. Se
méfier de la critique qui conduit à la réflexion qui conduit aux idées. S’engager pour les
pelouses et les espaces verts de son quartier, c’est faire la police soi-même et prévenir les
révoltes stériles qui pourraient naître de l’idée que ce quartier de chômeurs et d’immigrés
fabrique du malheur. S’engager c’est être positif et arrêter de pleurnicher sur ce qui ne va
pas !
Emmener des jeunes à coup de subventions pour repeindre une classe d’école au Mali (à
2 000 € le billet d’avion, c’est cher du pot de peinture), c’est les amener à « s’engager » et
c’est bien ! Les faire réfléchir sur l’état de délabrement de leur propre lycée poubelle, où
l’on a parqué la totalité des enfants d’étrangers pendant que les petits français ont obtenu
une dérogation pour le centre-ville, c’est leur faire faire de la « politique », et c’est mal !
S’occuper des ennuis des autres, c’est apprendre le désintéressement. Réfléchir à ses
ennuis, c’est faire de la politique.
La vie associative a accepté de faire vœu de chasteté politique en échange d’un
strapontin dans l’économie sociale marchande. Elle accueille, emploie et forme cette
nouvelle version des dames patronnesses que sont les « bénévoles » qui n’ont plus rien
des militants, qui s’engagent « concrètement » et grâce auxquels les services publics
délégués aux associations reviennent moins cher aux pouvoirs locaux. Elle apprend aux
jeunes à « s’engager positivement » en échange d’un contrat précaire. La vie associative
est aujourd’hui le plus fidèle allié et le plus efficace relais de l’idéologie droitière de
dépolitisation des relations sociales.
Exercice de traduction
En s’affranchissant des affiliations partisanes, les jeunes d’aujourd’hui renouvellent les
formes de l’engagement en parlant en leur nom propre et sur des sujets concrets.
La destruction des dynamiques populaires (syndicats, partis d’adhérents) par vingt ans de
libéralisme sauvage, laisse les jeunes isolés face à une compétition féroce dans une
société d’exclusion organisée de l’accès à la chose publique.
Page 15Formation
En 1971, la loi sur l’éducation permanente, grande victoire de l'après 68 arrachant le
savoir adulte des universités, se pervertit dans la « formation continue ». Le droit de
s’instruire à tout âge devient le devoir de se former toute sa vie, de se rendre employable
dans une société ultra productive et compétitive. Quand le chômage est un choix
d’organisation de l’économie, on se forme pour rester dans la compétition ! L’OCDE
déclare dans un rapport récent : « La décision politique d’encourager l’apprentissage tout
au long de la vie est destiné à fournir aux grandes entreprises européennes l’infrastructure
éducative qui est essentielle au maintien de leur taux de profit ». A l’heure où les
« qualifications » qui dessinaient des métiers font place aux « compétences » qui ouvrent
à des emplois, se former sans arrêt, c’est ne jamais avoir un métier. L’âge n’est plus
garant d’un savoir faire, mais c’est un handicap qui sera comblé par... l’envoi en formation
(ou le décrochage). Il faut « apprendre à apprendre » sur son temps de loisirs et à ses
frais, via des logiciels qui sont tout prêt à inonder le marché. Le droit à l’éducation tout au
long de la vie devient une obligation jusqu’à ce que mort s’ensuive de s’adapter
professionnellement à la destruction du travail : la plus belle sornette que l’on peut relayer
à gauche en croyant bien faire.
Exercice de traduction
Le droit à l’éducation tout au long de la vie offrira à chacun l’occasion de construire son
parcours personnel et d’en changer.
La destruction des systèmes éducatifs nationaux laissera chacun seul face l’obligation de
s’adapter au marché de l’emploi et d’abandonner toute carrière.
Page 16Gouvernance
Afin de dissimuler la relation de pouvoir et d'autorité, la « gouvernance », terme new-look,
laisse entendre que le gouvernement des choses et des gens, que la décision (publique
ou d'entreprise), ne sont plus qu'une affaire de gestion en bonne intelligence et en
participation avec le plus grand nombre. Dans la gouvernance il n'y a plus de chefs ni
d'autorité. Il n'y a plus que des procédures pour arriver ensemble au meilleur résultat. La
gouvernance est présentée comme un phénomène passif, et non comme une volonté
active : « ça » se gouverne tout seul, si on laisse faire (thèse libérale).
La grande astuce de la gouvernance locale, (par exemple) consiste de la part des
pouvoirs territoriaux à CHOISIR les heureux représentants qui seront dignes d'être
associés à la décision (ou de le croire) en échange d'un strapontin de reconnaissance.
Cela permet au maire ou au conseiller général de prétendre qu'il a associé la population,
quant il s'est contenté de désigner un ou deux vassaux associatifs trop heureux de poser
sur la photo. La gouvernance n'associe personne au partage du pouvoir, pourtant elle fait
tout comme.
Page 17Innovation
Dans l'accélération des idées, des biens et des services sur le mode du renouvellement
de la marchandise, l'innovation est une « méta-valeur ». Elle remplace toute les valeurs et
s'y substitue. Le sens est annulé, c'est la nouveauté qui fait sens. Bon ou mauvais,
(anciens critères) peu importe, c'est nouveau, donc c'est bien... même quand c'est
mauvais ! Le capitalisme n’étant pas un régime de satisfaction des besoins mais
d’accumulation sans fin, nous y sommes condamnés à « innover ». Manifester de la
nouveauté, sous le prétexte de « progrès » ou de « développement » c’est manifester son
dynamisme productif. Il n’y a pas de crime plus détestable que de « stagner », ou de se
contenter de quelque chose qui fonctionne bien ou qui donne entière satisfaction. La fuite
en avant dans la marchandise doit constamment réactualiser le discours de l’exploitation
pour mieux la masquer. Le capitalisme doit bouger ou mourir. Aucune vérité, aucune
valeur ne sont vraies au point qu’on ne puisse pas les dépasser, puis les démoder. Toute
valeur doit se renouveler pour rester crédible. La laïcité elle-même - par exemple - doit
devenir moderne. « L’innovation sociale, culturelle, pédagogique », imposent de déclarer
ringardes des manières de faire qui ont déjà plus de cinq ans. Il nous faut nommer d’une
manière nouvelle ce que l’on continue de faire comme avant. L’animation communautaire
doit s’appeler le développement local. La non-directivité doit laisser place à la pédagogie
de l’autonomie. Quand nous aurons enfin réussi à nous mettre d’accord pour abolir cette
abjecte notion de développement local, ou cette stupidité qu’est la pédagogie du projet, le
capitalisme aura depuis longtemps inventé de nouveaux mots pour réactualiser le masque
de sa domination.
Assez curieusement, le socialisme semble une idée définitivement ancienne quand le
capitalisme ne cesse d’être une idée d’avenir, constamment rajeunie. Le capitalisme est la
seule valeur qui ne se périme pas ! Le terrorisme de l’innovation produit la honte de
penser par nous même et de n’être pas modernes. Le crime de nous prendre pour des
sujets politiques plutôt que pour de bons petits agents du langage. Nous sommes
pitoyables à force de vouloir rester dans la course, et prouver notre modernité, l’actualité
de notre projet (reformulé comme il se doit), et de nos méthodes. Ce désir de modernité
dans le langage n’est rien d’autre que notre servilité constamment réaffirmée auprès de
nos maîtres financeurs. « L’innovation » ne vise que la « performance » qui n’est autre que
la « productivité » que la capital attend de chacun de nous. Depuis trente ans, c’est l’autre
nom de la déréglementation, de la destruction de toutes les lois que nous avions érigées
contre la loi de la jungle : innover, c’est alléger les freins mis à l’exploitation. Montrer qu’on
est innovant, c’est montrer qu’on est prêt à trahir, à renier tout ce à quoi on a cru avant. Le
moderne est un lâche. Quand aurons nous le courage d’affirmer la qualité durable de
notre savoir-faire et d’envoyer au diable les injonctions qu’on nous fait à présenter des
« projets innovants » ?
Exercice de traduction
Le nouveau projet d’établissement fera la place à une innovation pédagogique qui visera à
une plus grande autonomie de l’élève.
La mise en concurrence commerciale des écoles leur permettra de s’affranchir de l’égalité
et de laisser les élèves encore plus seuls dans la sélection et la compétition.
Page 18Interculturel
1. Présentation pacifiée et non politique des rapports internationaux.
2. Ethnicisation des rapports sociaux, et transformation de la lutte des classes en un
problème de compréhension mutuelle entre différents codes culturels. « Faire de
l'interculturel », dans un quartier, c'est œuvrer à une « meilleure compréhension » entre
des identités culturelles devenues non-politiques, c'est gommer le conflit, les raisons du
conflit, c'est interdire l'explication politique des souffrances. Implicitement, le problème ce
ne sont plus les ouvriers, ce sont les Arabes, les étrangers, les autres...
Page 19Jeunesse
« La jeunesse », ça n’existe pas. Personne ne peut en produire une définition correcte.
C’est impossible.
« La jeunesse » est un adolescent Algérien qui habite à la Courneuve et un étudiant de 24
ans à sciences po qui habite Neuilly. Pourtant cette notion apparaît dans le discours
politique à partir des années soixante (les jeunes qui traînent et ne travaillent pas) et
permet de masquer le vrai problème : à quelle condition fait on partie de cette société
aujourd’hui, et comment s’y intègre-t-on ? Chômage, coût de la vie, pénurie de logements,
ségrégation scolaire, etc... Transformer une catégorie politique (les conditions d’accès au
marché de l’emploi) en une catégorie naturelle (les jeunes) est une astuce formidable pour
dépolitiser tous les problèmes. La « jeunesse » devrait être une image pour parler des
forces de la transformation sociale, et non pas une catégorie sociologique spécifique de la
population à « traiter » séparément.
Une « politique de la jeunesse » devrait englober entre autres ministères : l’école, la santé,
les affaires sociales, la culture, les loisirs, le logement, l’accès au travail, etc, etc, etc... Le
seul fait que la politique de la jeunesse n’englobe même pas la question de l’éducation et
de l’école en dit assez long sur le joujou qu’on agite sous notre nez. Pour cela, les
associations d’éducation populaire ont deux défis : Arrêter de ne de proposer aux jeunes
que du rap et du sport et développer effectivement leur expression et leur délibération sur
des enjeux non-jeunes (école démocratique, monde du travail, services publics, droits
sociaux et politiques, stratégie internationale de la France...) et susciter dans le même
temps l’expression et la délibération des citoyens adultes sur les politiques de la jeunesse.
Il faut dépasser l’échelle locale et l’échelle du traitement sectoriel (non-politique) des
problèmes (les jeunes, les vieux, les handicapés, les Arabes, les femmes...).
On prendra alors le risque de s’apercevoir : que la démocratie n’est jamais locale (on ne
fait pas de démocratie avec des problèmes de boîtes à lettres, mais avec des problèmes
généraux, on n’en fait pas en s’occupant de ses problèmes mais en s’occupant de ceux
des autres) et la démocratie n’est jamais sectorielle (on ne fait pas de la démocratie
qu’entre femmes, ou qu’entre jeunes, ou qu’entre immigrés...).
Car « la jeunesse », c’est aussi la question de l’immigration, de la vieillesse, de la féminité
ou du handicap, et de tout ce qui n’est pas le mâle blanc diplômé d’origine française, âgé
de quarante ans, (la seule catégorie massivement intégrée, et munie d’un contrat de
travail à durée indéterminée, dixit l’INSEE). C’est la question qui permet d’interroger le
rapport entre « production de la société et reproduction de la société ».
Les jeunes permettent de voir, comme les vieux, comme les immigrés, comme les femmes
et comme les personnes invalides, en quoi un système est non-reproductible, à
développement non-durable des rapports sociaux, non-intégrateur, et donc désintégrateur
de ses principales sources de légitimité et d’efficacité. Le problème n’est pas de traiter les
jeunes en tant que problème séparé, marginal, du dysfonctionnement temporaire de la
société, mais de se saisir des jeunes comme révélateur du problème global de la société.
On vous apprend des savoirs, on vous transmet connaissance et culture. Mais ensuite,
quand vous n’êtes plus jeunes, connaissance et culture sont détruites dans le processus
de déqualification à l’embauche, par la massification du chômage...
Les animateurs de jeunesse sont payés pour retarder le plus longtemps possible le
moment où les jeunes vont s’intéresser aux problèmes des adultes et faire de la politique.
Détourner les jeunes de la politique, c’est à cela que sert l’animation « basket and rap » !
L’union européenne s’est fixée un objectif à Lisbonne : « être l’économie de la
Page 20connaissance la plus compétitive ». Avez-vous besoin d’une traduction ? Il s’agit de
gagner la compétition capitaliste mondiale contre les États-Unis et contre la Chine. Pour
cela, il faut dans dix ans des travailleurs les plus pointus, les plus formés possibles, et
laisser tous ceux qui ne suivent pas et qui retardent sur le bas côté pour la voiture-balai du
social. L’idéologie que les jeunes doivent intégrer comme une drogue est celle de la
compétition, du dépassement de soi, de l’esprit d’entreprise, de l’individualisme, du projet.
« Oui mais les jeunes demandent eux-mêmes du rap et du sport ! » Dans les réserves
d’indiens, les indiens demandent à être dans des réserves d’indiens. Parce qu’on ne leur a
laissé pour seul horizon, que des réserves d’indiens ! Disons leurs : vous êtes ici dans une
zone réservée. En dehors, il y a des zones non-réservées, ce sont toutes les terres qu’ils
vous ont prises. Tout ce qui fait qu’ils partagent le sort des vieux, des moins qualifiés, des
femmes et des étrangers...
C’est tout le contraire que de leur proposer de « s’insérer » avec du basket-ball !
Construire le problème de l’intégration critique, avec une bande de jeunes, c’est leur
proposer d’étudier le marché de l’emploi local : « qui est en dehors du marché de
l’emploi ? Une proportion anormale de filles, d’autant plus qu’elles ont plus de diplômes
que les garçons. Tiens ! Ce n’est donc pas un problème de jeunes, mais un problème
hommes / femmes. Oui, mais ils sont étrangers. Tiens, c’est un problème de
ségrégation ? ».
Si l'État veut, à ce point, qu’on occupe des jeunes, c’est parce que la puissance publique
(en particulier communale), persiste à pratiquer une culture politique d’exclusion et de
réduction des conflits. Surimposer la paix sociale dans des conditions où elle n’est pas
plausible, suppose de démultiplier les efforts de dispersion de toute coalition possible.
Faire croire que les jeunes ont des problèmes spécifiques qui ne sont pas ceux des
adultes !
Exercice de traduction
Il faut mieux prendre en compte la culture jeune dans les quartiers.
Il est essentiel que les personnes libérées du système scolaire et sans aucune
perspectives d’emploi occupent leur temps oisif dans des activités qui ne les fasse pas
réfléchir au fonctionnement de la société.
Page 21Libre et non faussé
Magie du langage : dans le projet de traité établissant une constitution pour l’Europe
soumis au référendum du 29 mai 2005, « L'Union offre à ses citoyens (...) un marché
intérieur où la concurrence est libre et non faussée. » (Art. I-3) Somme toute, un paradis
de liberté... pour qui ?
Une « concurrence libre » signifie que l’Union – (c'est-à-dire les 25 ministres des finances)
– organise un espace ou RIEN ne doit pouvoir échapper à l’argent et au rapport
marchand.
Une « concurrence libre » signifie qu’aucun pouvoir privé ne doit être empêché de
s’intéresser commercialement à aucun secteur de l’activité humaine, ni à aucun bien
commun universel (sports, loisirs, éducation, culture, tourisme, santé, environnement, air,
eau...), qui jusque là n’étaient pas considéré comme commerciaux. Il s’agit de TOUT
transformer en marchandise. Toute relation entre deux êtres humains doit pouvoir – d’une
façon ou d’une autre - donner lieu à une transaction d’argent. Il n’y a plus aucun frein à
l’appétit de commerce.
« Fausser la concurrence » fait penser à une mauvaise action. Mais qui sont les
« faussaires » du langage ? « Fausser la concurrence » veut dire « protéger » les faibles,
« garantir » le droit des fragiles, « aider » ceux qui en ont besoin, « corriger » les
inégalités, « respecter » l’environnement, « se soucier » de l’intérêt général, « offrir » des
services gratuits, « assurer » une éducation gratuite, une santé gratuite...etc.
Toute l'œuvre de la civilisation humaine depuis des millénaires consiste à fausser la
concurrence, c'est-à-dire à protéger le faible contre le fort, à créer du droit pour empêcher
le commerce de s’approprier tout n’importe comment, à créer des services publics, à
mettre des barrières aux intérêts privés au nom de l’intérêt général.
Dans une concurrence « libre et non faussée », les services mis en œuvre par la vie
associative devront pouvoir entrer en concurrence avec des firmes privées, et toute
subvention à cette vie associative devra disparaître. Bel avenir pour la démocratie.
Dans la constitution française, les droits fondamentaux étaient la liberté, la propriété, la
sûreté et la résistance à l’oppression. Dans la constitution européenne ce sont : la libre
circulation des personnes, des services, des marchandises et des capitaux, ainsi que la
liberté d'établissement... pour les firmes étrangères, sans avoir à respecter les législations
sociales en vigueur (le retour discret du fameux A.M.I) (Art.I-4).
On réfléchit, on relit, on se frotte les yeux et on se dit que l’on a mal lu. Aucun être humain
sensé, doté d’un cerveau en état de fonctionnement, ne peut désirer vivre dans un espace
où le concurrence serait « libre » et « non faussée », c'est-à-dire où la loi de la jungle
serait devenue une règle constitutionnelle, et la loi du plus fort et du plus riche
s’imposeraient aux États qui n’auraient plus le droit de protéger les faibles. Aucun être
humain doté d’un cerveau (et d’une mémoire) ne peut approuver ce projet néo-fasciste
face à une Union Européenne qui réaliserait le rêve fou de l’utopie ultra libérale d’un
capitalisme totalitaire à faire pâlir d’envie les Américains. Nous désirons au contraire une
constitution qui s’engage à fausser la concurrence. Encore plus, qui nous explique
comment elle va la fausser, en nous donnant des garanties d’application. Copie à revoir.
Page 22Lien social
Au chapitre des nouveaux mensonges qui permettent de masquer la demande sécuritaire
d’ordre et de soumission, l’appel au « lien social » incite à apprendre aux pauvres et aux
jeunes à bien se tenir, à « jouer le jeu » et à limiter leur rébellion malgré leur absence de
perspective de travail et d’avenir. Il n’existe pas de société sans lien social, mais des
sociétés où l’on réprime le lien politique. Le seul lien qui fasse société est une discussion
conflictuelle sur l’intérêt général, pas une pacification généralisée des rapports sociaux de
domination. L’épouvantail de la désagrégation du lien social masque l’absence
d’intégration par un salariat systématiquement attaqué. Il faut désormais adhérer à la
société de marché plutôt que de viser un idéal d’émancipation et de changement.
S’insérer en silence plutôt que de s’intégrer par la participation au conflit sur la
redistribution du travail. L’absence de conflit, écrasée par le discours sur la cohésion
sociale est bien plus dangereuse pour la démocratie que les conflits autour du travail. Or
les pauvres sont au contraire sur-intégrés par un modèle terroriste de consommation à
laquelle leurs revenus ne leur permettent pas de prétendre. Qui est le plus nuisible pour le
lien social sur un quartier ? Le jeune désœuvré ou la patron de Michelin qui licencie 7 000
ouvriers et détruit 7 000 familles au plus fort de ses bénéfices boursiers ? Qui triche ? Qui
joue le jeu ? Qui accepte la règle et qui la détourne ? Si le lien social est un ensemble de
règles et de contraintes acceptées au nom de l’intérêt général, le rappel à la règle,
sermonné par nos vertueuses associations socioéducatives au nom de la citoyenneté
(droits et devoirs) semble curieusement ne s’adresser qu’aux pauvres !
Exercice de traduction
Les associations doivent mobiliser les énergies citoyennes pour la réparation du lien social
mis en danger par les incivilités.
Les nouveaux alliés de la domination doivent faire taire la colère des victimes du
libéralisme en leur faisant abandonner leurs droits et se soumettre à des devoirs sans
contrepartie.
Page 23Mixité sociale
Le rêve de coexistence pacifique des petites gens et des classes moyennes, celles-ci
tendant la main à celles-là dans une ascension sociale inéluctable, s'est effondré avec
l'autorisation politique donnée aux dirigeants d'entreprise de se comporter comme des
prédateurs, à partir des années 80. Avec la vague de déréglementation mise en œuvre
par les « socialistes », le partage de richesse des années soixante (70% aux salaires et
30% au capital) a reculé (60% au salaires et 40% au capital aujourd'hui), avec une
richesse qui a doublé dans le même temps. Soit un recul de 10 points en une génération,
10% de la populations interdite d'emploi et l’apparition de nouveaux riches.
Les prolétaires sont parqués dans des cités-ghettos (pas encore des camps), et la
situation des quartiers est insurrectionnelle (émeutes nationales de novembre 2005).
L’usage du mot « ghetto » et la référence inquiète aux États-Unis sont deux des principaux
poncifs du discours sécuritaire, et plus largement de presque tous les discours consacrés
aux « banlieues ». La concentration de « populations à problèmes » (c’est-à-dire de
pauvres et d’immigrés) dans des territoires nommés « quartiers sensibles » est en effet
présentée comme la source de tous les maux. Et à ce diagnostic est souvent opposée une
solution-miracle : la « mixité sociale », née en mars 1972 d’une circulaire d’Olivier
Guichard, alors ministre de l’Aménagement du territoire. L'idée n'est pas de faire reculer
l'inégalité en s'attaquant à l'organisation du travail dans les entreprises (renforcer le droit
du travail au lieu de le détruire, cesser de défiscaliser les entreprises, enrayer les
exonérations de cotisations) mais de disperser les concentration de pauvres en
démolissant les cités et en n'en relogeant qu'une partie (les autres vont plus loin) ou de les
« diluer » en mélangeant les classes dangereuses avec des classes moyennes sages,
soumises, intégrées par le rapport salarial.
Le cynisme de cette philosophie est tellement énorme que chacun prétend ne pas le voir,
et préfère traiter le symptôme plutôt que la cause : s'il y a des pauvres, dispersons les,
invitons quelques enseignants à habiter les ghettos en les culpabilisant s'ils s'offusquent
de quelques incivilités somme toutes bien supportables : drogue, violence, inactivité,
économie parallèle, trafics pour survivre, délinquance organisée... pas un seul élu
municipal n'habite les quartiers en France : messieurs les bons apôtres, donnez donc
l'exemple de la mixité sociale ! Louez votre pavillon à une famille africaine et venez
habiter une tour ! Et pendant que vous y êtes, faites respecter la carte scolaire, la seule
mesure réellement productrice de mixité sociale en France, celle qui interdit que l'on
choisisse son école ou les fréquentation de son enfant ou commencez par la respecter
vous-mêmes : les élus municipaux sont les premiers à demander et à obtenir des
dérogations à la mixité sociale pour leurs enfants, et Ségolène royal propose même
d'abolir cette insupportable contrainte républicaine et égalitaire.
Jamais le tissu urbain ne fut structuré autrement que sous la forme d’une stratification
opposant les quartiers riches et les quartiers pauvres. Aujourd’hui, le processus s’est
encore aggravé, les communes qui détruisent les cités HLM investissent non dans de
nouveaux logements sociaux, mais dans l’accession à la propriété. Si l’urbanisme exprime
la manière de penser d’une époque, on peut s’inquiéter d’une France se dirigeant tout
droit vers la division spatiale en trois ghettos : celui des riches, celui des pauvres et celui
des classes moyennes.
Le concept de mixité sociale ne traduit pas la volonté de mettre fin au fait qu'il y ait des
pauvres et des riches, mais le fait qu'il faille les faire coexister pacifiquement. C'est une
résignation qui trahit le renoncement à combattre l'inégalité. Il s’agit bien de réaliser
« une gestion harmonieuse de la misère » !
Page 24Mais il y a aussi des raisons intéressées à la discrimination. Pour beaucoup de bailleurs,
la valeur du parc immobilier dépend de l’image de la cité.. qui dépend elle-même de
l’image des locataires... qui dépend elle-même de l’origine géographique... etc... Pour
beaucoup de municipalités, il faut freiner l’arrivée dans le logement social des populations
les moins « désirables » - c'est-à-dire les familles les plus pauvres, les plus précaires, les
plus nombreuses et les plus « ethniquement différentes ». Cependant les choses ne
peuvent pas se dire aussi ouvertement car la sélection des locataires en fonction de leur
origine reste, aux yeux de la loi, une discrimination. Il faut donc parler à mots couverts, ce
que permet justement le terme de « mixité sociale ». L’union des fédérations d’organismes
HLM a ainsi recommandé d’utiliser ce concept de « mixité sociale » pour motiver des refus
d’attribution et ne pas risquer d’être attaqué pour discrimination.
Exercice de traduction
Prolétaires de tous les pays, rassemblez vous !
Prolétaires de toutes les communes : dispersez-vous !
Page 25Partenariat
« L'armée, premier partenaire des jeunes » peut-on lire sur une affiche de recrutement de
l'armée de terre... mais aussi les piles Machin qui sont devenues les « partenaires de nos
loisirs », ou encore les associations qui osent qualifier de « partenaires » les mairies ou
les services de l'État qui les financent et qui sont véritablement leurs donneurs d'ordre !
Comme l'écrit Fabrice DHUNE, il faut saluer ce coup de maître qui transforme son propre
serviteur en son égal ! Le recours au partenariat fait disparaître la référence hiérarchique,
et disloque complètement le rapport de domination...
Tous dans le même bateau ! L'idée centrale est que la banalisation du terme, en
opposition radicale avec le sens du principe de partenariat, sert une virtualisation de la
relation, contribuant à rendre impossible la construction d'une effective relation de
partenariat. La déconflictualisation des rapports sociaux qui se manifeste dans ce
retournement participe tout à la fois du désenchantement de la politique (du point de vue
de l'espace public) et de l'implication professionnelle (du point de vue de l'espace privé) :
deux faces qui servent l'ancrage en profondeur de l'idéologie (néo)libérale sous les
apparences du consensus et de l'évidence.
Page 26Participation
La démocratie participative s’oppose en principe à la démocratie représentative, mais la
confusion est complète. Chaque fois que vous réclamez plus de participation, c’est en
réalité plus de délégation que vous désirez : (un nouveau « conseil » de jeunes, de
femmes, d’habitants, d’immigrés, d'handicapés, une nouvelle association, une nouvelle
plateforme, etc...). Êtes-vous favorable à l’évaluation des enseignements par les élèves
eux-mêmes (et non leurs représentants) ? Non ! A celle des programmes scolaires par les
enseignants eux-mêmes ? Non ! A celle des politiques de logement des OPHLM par les
habitants eux-mêmes ? Non ! A la marche de l’hôpital par les infirmières elles-mêmes ?
Non ! Tout le monde est pour la participation, à condition de ne participer... à rien !
Dans le nouveau système de « gouvernance » locale ou nationale, le pouvoir désigne des
associations, convoquées en tant qu’expertes, chargées de représenter des intérêts
particulier et de valider des décisions politiques auxquelles elles sont faussement
associées. L’impression de démocratie est sauve et le pouvoir peut prétendre qu’il a
consulté, associé, fait « participer », mais le pouvoir de modifier la politique définie en
amont est à peu près nul ! Toute autre serait la revendication de construire de la
délibération publique, contradictoire, conflictuelle, autour de la politique en cours
d’élaboration, en y associant les usagers destinataires, les agents associatifs, les
fonctionnaires chargés de l’appliquer, les élus politiques chargés de l’imaginer, et les
simples citoyens considérés comme autre chose que des clients. Il ne s’agit pas de
démocratie directe sur le mode du sondage (« ô, jeunes, que voulez-vous ? »), mais de
processus longs et rigoureux, s’apparentant aux méthodes de la recherche-action ou de
l’enquête sociale, associant toutes les catégories concernées par une politique publique,
et permettant d’en retravailler les contradictions vers plus d’intérêt général, moins
d’inégalités.
Exercice de traduction
Le conseil de participation des habitants a accueilli positivement l’embauche d’emplois-
jeunes médiateurs locaux du lien social.
Les représentants validés du pouvoir local ont fait taire les contradictions d’une mesure
aussi aberrante, anti-pédagogique et discriminatoire que celle consistant à demander sans
qualification professionnelle à des immigrés de la deuxième génération de calmer les
immigrés de la troisième génération pour la tranquillité du centre-ville.
Page 27Professionnel
Dans les années 80, l'idéologie du « management » d'entreprise s'est diffusée à toute la
société. La « qualité professionnelle » de nos moindres gestes est devenue la norme et
une écrasante pression psychologique nous est tombée sur la tête : quand le critère
« professionnel » devient le standard de l'évaluation d'une action, alors « amateur », (ou
bénévole) devient synonyme d'incompétence, de médiocrité, de moindre qualité.
Avec le « zéro défaut », le « just in Time », le « flux tendu », la norme « ISO 9002 » ... la
perfection productive devient une forme de fascisme. Celui qui ne donne pas le meilleur
de lui-même à la grande concurrence capitaliste n'est pas digne de faire partie de cette
société. C'est un parasite, bientôt un sous-homme qui nous fait prendre du retard dans la
grande et impitoyable compétition mondiale..
On a vu des animateurs jeunesse, des travailleurs sociaux, des comédiens amateurs, des
élus bénévoles, sommés d'adopter un comportement vraiment « professionnel », devoir
justifier une qualité « professionnelle » et des résultats « professionnels »... Cette pression
psychologique qui s'est déguisée sous l'argument de la qualité n'était rien d'autre que
l'exigence de la compétitivité, de l'hyper-productivité qui ne dit pas son nom. Quand toute
une société est sommée de n'être plus composé que de professionnel, il n'y a plus de
société, il n'y a plus que des robots soumis à la grande loi de la marchandise de la
rentabilité et du profit. Il y a aujourd'hui des parents professionnels (professions parents)
des habitants professionnels, et des bénévoles associatifs qui ne sont plus que des
professionnels non-salariés, au regard des compétences qu'on leur demande de
maîtriser : gestion, politique, montage de projet, évaluation d'action, négociations
partenariales, ...
Avec la tyrannie du tout professionnel, arrive bientôt celle du tout-évaluation, qui a pour
but de dépister et d'éliminer toutes déviances improductives. Tout comportement non
professionnel est donc anticapitaliste.
Résister, c'est plaider pour le droit à l'erreur, au tâtonnement, à l'imperfection, à
l'hésitation, au recommencement, à l'à-peu-près, à la paresse, à l'amusement, à l'échec, à
l'improductivité... c’est refuser d’évaluer et d’être évalué selon des normes
« professionnelles ».
Exercice de traduction
L’animateur de l’atelier « peinture » du centre de loisirs doit viser une production de qualité
artistique professionnelle avec les enfants.
Le travailleur social n’est pas là pour que les enfants s’amusent ou s’expriment, mais pour
qu’ils intègrent l’exigence de rentabilité et de productivité qu’on attendra d’eux dans le
travail.
Page 28Projet
Au hit-parade des mots insoupçonnables, des aliénations merveilleusement positives
ancrées au plus profond de nos convictions se tapit le plus redoutable d’entre eux :
« PROJET », le cœur de la nouvelle culture capitaliste. Le « projet » apprend à travailler
seul, à viser une production, c'est à dire à réaliser un produit. Le projet détruit le temps et
le long terme. Il a un début et surtout une fin. Il est remplaçable par un autre. Pour le
pouvoir (très friand des projets), il transforme des relations politiques en relations
marchandes car il permet d'acheter des prestations-produit en les déguisant en
démarches. En management, « projet » remplace « hiérarchie »... Avec l’envahissement
de la culture du « projet » depuis une vingtaine d’années le capitalisme révèle le cœur de
son idéologie : une société qui n’a plus de projet (de transformation sociale vers plus
toujours plus d’égalité) et dont la volonté politique semble se résigner au règne de la
marchandise pour cause de concurrence, de mondialisation, et d’écroulement des
socialismes « réels », ne cesse de demander à ses citoyens (et surtout aux plus pauvres
privés d’avenir) de se projeter, de faire des projets, (des micro-projets d’adaptation). Une
société qui ne se projette plus dans l’avenir mais aménage à l’infini le présent de la
marchandise dans un capitalisme pour seul horizon dispense des miettes de futur sous
forme de micro-projets où chacun devient petit capitaliste de sa vie.
S’engager dans un « projet » c’est manifester son dynamisme, son esprit d’initiative, son
adhésion à ce système compétitif et parcellisé... ce n’est pas critiquer, ni militer, ni douter.
Le « projet humanitaire » remplace le combat politique. Il est évaluable immédiatement et
remplaçable par un autre projet. Il annule le long terme et la nécessité de s’intégrer
durablement à un collectif. Dans leur étude sur le « nouvel esprit du capitalisme », Eve
Chiapello et Gérard Boltanski montrent que si le mot « hiérarchie », qui venait en tête des
mots employés dans des ouvrages de management dans les années soixante, a
complètement disparu des années quatre-vingt dix, en revanche, le hit-parade
contemporain du management de l’entreprise capitaliste (nombre de fois cité dans le
même ouvrage) revient sans conteste à « PROJET ».
En engageant les jeunes dans des dynamiques multiples de projets, les travailleurs
sociaux leur apprennent à morceler leurs désirs, leurs vies, leurs idéaux. On leur interdit
de n’avoir qu’un projet qui durerait toute une vie : vocation, métier, mariage... et on les
dresse à l’éphémère, à la mobilité, à l’employabilité d’eux-mêmes dans un monde
présenté comme instable et qui n’a jamais été aussi stable : de la stabilité de la
marchandise capitaliste pour toujours, pour tous et en tous lieux. Pratiquer une
« pédagogie par projets » c’est enseigner l’adaptation au court terme, et la renonciation
aux idéaux qui structurent une vie, une personne, un groupe social. Dans cette nouvelle
exigence/oppression/aliénation, le perdant, l’exclu est celui qui n’est pas engageable dans
un projet ou qui se montre incapable de changer de projet (celui qui se cramponne à un
idéal). L’intérêt du projet est d’avoir un début et une fin (surtout une fin). Une fois celle-ci
atteinte, on dissout l’équipe, sa subvention, la dynamique et on est prêt pour un tout autre
nouveau projet, avec de nouvelles têtes et de nouveaux financements. On vous a déjà vu
l’année dernière, vous n’allez pas vous abonner, laissez la place à d’autres et tentez votre
chance ailleurs ! Sous l’intitulé de « projet », le pouvoir achète en réalité des « produits »
et prépare le futur travailleur aux nouvelles règles du management libéral, de la
marchandise, de la rotation des tâches et de sa propre employabilité. La pédagogie par
projet, la subvention au projet, est l’apprentissage du nouveau management, de la rotation
accélérée des produits et des marchandises sociales, de la précarité et de l’incertitude
acceptées par avance : soyons aventureux dans un monde instable que diable, et que les
faibles perdent ! Il n’y a pourtant qu’un seul « projet » qui vaille (quel autre ?) : la
Page 29Révolution.
Exercice de traduction
Dans les zones à redynamiser, les chefs de projets financent des projets d’habitants dans
le cadre de contrats d’objectifs.
Dans les groupes sociaux à réduire au silence politique, les chefs de produits du nouveau
marketing public achètent la paix sociale des habitants en renouvelant au coup par coup
leur dépendance financière au pouvoir.
Page 30Qualité
Dans le monde du travail social, éducatif et culturel, un Ovni étrange (et étranger) a
envahi notre espace mental et institutionnel : la « démarche qualité ». Coïncidence ? Le
passage du statut de l’usager à celui de client dans un espace de moins en moins public
va de pair...
Réservée jusqu’alors à la fabrication des aspirateurs ou autres épluches-légumes avec
engagement de satisfaction du client, elle s'est peu à peu étendue aux services privés,
puis – et c'est beaucoup plus grave -, aux services publics !
L'intérêt général se transforme ainsi subrepticement en un marché d'intérêts privés, grâce
toujours à la même manipulation du langage : qui peut être contre la « qualité » ? Y a-t-il
un seul fou parmi nous qui s'opposerait à la notion de « qualité » ...? C'est bien là le
problème : ne pouvant pas nous y opposer, nous ne pouvons pas non plus nous opposer
à ce que cette démarche importe pour l’installer dans le champ éducatif et culturel : la
marchandisation des rapports sociaux, éducatifs et culturels.
Quand le service public du téléphone, conçu pour des « usagers » dotés de droits égaux
qu'il fallait garantir, se transforme en opérateurs privés de téléphonie, s'adressant
désormais à des clients dotés de désirs et de pouvoirs d'achat inégaux à « satisfaire », la
signification même de l'accès à l’acte de téléphoner devient un produit, une marchandise,
le support d'un profit, au détriment d'une fonction sociale à faire exister également pour
tous.
Dans le champ de la jeunesse ou du travail social, la « démarche qualité » transforme les
jeunes en clients invités à juger (pardon – à évaluer) la qualité du service rendu. Le projet
philosophique, l'intention éducative disparaissent au profit d'une logique de prestation qui
interdit l'expérimentation, le tâtonnement, le droit à l'erreur et l'échec. Elle installe des
« protocoles » d'efficacité et de rationalisation des tâches qui excluent la part d'humain.
Or, il n'existe pas de processus éducatif sans droit à l'échec. La démarche qualité n'est
pas seulement « axée résultat » elle est orientée « résultat positif ». Elle transforme
l'obligation de moyens du travail social en obligation de résultat. Elle détruit donc sa
propre finalité éducative, elle anéantit le sens même de l'acte éducatif. Le fruit est mûr
pour être cueilli par le marché privé.
Récemment imposée au domaine médico-socio-éducatif et à des pans de plus en plus
importants de l'Éducation spécialisée (avec normes ISO 2002), la démarche qualité
dépossède les acteurs des critères de l'évaluation de leur acte éducatif, au profit d'un
management stérile qui permet de traiter l'intervention comme n'importe quelle prestation,
avec les méthodes et critères de « gestion des ressources humaines » adéquats ! Quels
sont ces critères ? La course effrénée dans une compétition où être le premier en dépit de
toute possibilité de rencontre humaine isole chacun dans un complice renoncement à
toute avancée de civilisation.
Exercice de traduction
Le centre de loisirs a adopté une démarche qualité en vue de mieux satisfaire les parents.
La direction du centre de loisirs veillera à ce qu'aucun débat de fond sur l'acte éducatif ne
puisse venir perturber la relation de clientèle établie avec les parents-consommateurs.
Page 31Responsabilisation
Dans les années soixante-dix, (avant la contre-révolution libérale qui allait attaquer un par
un tous nos droits), les associations qui travaillaient avec des jeunes voulaient les
« émanciper ». Aujourd’hui on les entend plus parler que de les « responsabiliser ».
Qu’est-ce à dire ?
Quand il y avait du travail pour tout le monde, il s’agissait de s’épanouir et de ne pas
s’aliéner au travail. Puis la France a doublé sa richesse en 17 ans, et trois millions de
chômeurs sont apparus. Le partage de cette richesse se faisait autrement. Il fallait alors
apprendre à certains à abandonner des droits, à faire des efforts, à adopter la rigueur, à
brader leurs retraites, à se soigner moins, à accepter de se serrer la ceinture, à accepter
de perdre ce qu’ils avaient conquis pour que d’autres puissent s’enrichir encore plus vite.
Se responsabiliser, pour les pauvres, c’est se soumettre à la logique de prédation des
riches. Responsabiliser quelqu’un c’est l’obliger à accepter une logique qui va contre ses
intérêts.
« Dominique de Villepin annonce son intention de
responsabiliser les parents des
enfants en grande difficulté scolaire à travers la signature d'un « contrat de
responsabilité » obligatoire, sous peine d'amendes, d'une mise sous tutelle ou d'une
suspension du paiement des allocations familiales » (Reuters).
L’ Union Européenne demande : « comment responsabiliser les États-membres pour
promouvoir la compétitivité » ?
Un consultant propose : « Comment responsabiliser votre équipe et développer des
comportement managériaux favorisant la cohésion d'équipe, l'autonomie et la
responsabilité ? »
« Quand on objecte au Premier ministre Balladur que la réduction des prélèvements
obligatoires se traduira forcément par un recours volontaire à des assurances
complémentaires, il répond que c'est bien ce dont il s'agit : responsabiliser le citoyen en lui
laissant le soin d'arbitrer entre différents types de dépenses ». (L’expansion)
« Dans un entretien accordé au Journal du Dimanche, le premier ministre, Jean-Pierre
Raffarin, a fait savoir que, lors de chaque consultation, le patient devra mettre de sa poche
« un ou deux euros ». Cette mesure a pour but de responsabiliser les Français ». (Le
journal du dimanche).
Pourtant, les 22 milliards d’euros non reversés à la sécu par les entreprises couvriraient
largement les 11 milliards de déficits pour lesquels on demande aux malades de se
responsabiliser et les exonérations de cotisations patronales couvrent largement le trou de
l’assurance chômage.
Le discours de la responsabilisation est obscène. Il consiste à faire peser sur les individus
la responsabilité des comportements prédateurs des classes dirigeantes. Aucun éducateur
digne de ce nom ne devrait aujourd’hui entonner ce refrain.
Exercice de traduction
Les jeunes doivent se responsabiliser et rentrer dans des logiques d’insertion sociale et
professionnelle.
Les jeunes à qui l’école a promis un avenir mais qui n’en ont aucun, doivent se taire et
accepter des emplois déqualifiés dont un chien ne voudrait pas.
Page 32Solidarité
La contradiction entre le « tout-liberté » à l’américaine (sans l’égalité), et le tout-égalité à la
soviétique (sans la liberté), tente de se résoudre en France dans la présence de droits
collectifs qui s’expriment dans la triangulation républicaine Liberté – Égalité – Fraternité.
Du point de vue des républicains, l’idée de la fraternité signale qu’on ne se choisit pas,
mais que l’on fait partie d’une société, (et non d’une communauté), comme dans une
famille où l’on ne demande pas à un frère s’il veut bien avoir l’obligeance d’être
« solidaire » de ses autres frères et sœurs.
Au contraire, le modèle anglo-saxon de la liberté, toute la liberté, rien que la liberté
s’oppose à l’imposition de droits collectifs qui sont vécus comme des freins insupportables
à la logique du contrat libre entre les individus. Dans ce modèle, par exemple, l’éducation
n’est pas un droit collectif, mais une simple rencontre entre une offre privée d’éducation et
une demande privée d’éducation. Elle peut donc faire l’objet d’un commerce.
La solidarité suppose le registre du volontariat. Nul n’est obligé d’être solidaire. Il s’agit
essentiellement d’une attitude individuelle, proche de la charité. La fraternité en revanche
impose le principe de droits collectifs, ou si l’on veut, d’une « solidarité obligatoire »,
imposée, non volontaire, non libre et non négociable.
Or ce vieux terme de la gauche ouvrière, la « solidarité », est aujourd’hui récupéré par la
droite et détourné de son sens. L’intérêt de la manœuvre est de culpabiliser la victime et la
société civile. Si les choses vont si mal, c’est du fait d’un manque de solidarité entre les
gens (donc entre les victimes de l’exploitation elles-mêmes). Les gens sont devenus si
« individualistes » ma brave dame ! nous répète sans fin un système qui a tout fait pour
casser les organisations collectives. Le patron de Michelin qui licencie 7 000 ouvriers
l’année de ses bénéfices record n’est pas un « individualiste », lui !
Dernier forfait en date de cette escroquerie intellectuelle, la journée de la solidarité avec
les personnes âgées. Après avoir défiscalisé à mort les patrons, exempté les entreprises
de cotisations sociales (pour les vieux, donc...), le gouvernement demande aux
travailleurs de rendre une journée fériée à la Pentecôte et d’augmenter d’une journée de
travail les profits des entrepreneurs.
L’appel incessant à la solidarité, thème faussement généreux, vise à détruire le sentiment
d’un destin commun encadré par des droits collectifs, au profit d’une philosophie du
contrat dans laquelle on fait appel à l’engagement sélectif des uns au profit des autres. A
cette philosophie correspond la montée de l’humanitaire comme corollaire à l’effondrement
du politique. Il s’opère alors un transfert de responsabilités de l’État vers la société civile :
la puissance publique qui détruit les garanties sociales, le droit du travail, qui prône une
politique monétaire, qui fait flamber la bourse et qui défiscalise les entreprise, encourage
dans le même temps une « économie solidaire » qui confie aux associations le soin de
réparer les dégâts de ses choix politico- économiques.
Quand j’entends le mot « solidarité », je sors mon code du travail !
Page 33Bibliographie
La novlangue néo-libérale. La rhétorique du fétichisme capitaliste. Alain BIHR. Editions
Page deux, collection «Cahiers libres». 2007
Cent mots pour résister aux sortilèges du management. Gérard LAYOLLE. Editions seuil,
collection «les empêcheurs de penser en rond »2008
Le nouvel esprit du capitalisme. Luc Boltanski et Eve Chiapello. Gallimard. 1999
Les nouveaux chiens de garde. Serge Halimi. Raisons d’agir. 1997
Les évangélistes du marché. Keith Dixon. 1998
Les nouveaux mots du pouvoir. Abécédaire critique. Sous la direction de Pascal Durand.
Editions Atlan. 2007
Le pouvoir des mots. Politique du performatif. Judith Butler. Editions Amsterdam. 2004
La barbarie douce. La modernisation aveugle des entreprises et de l’école. Jean-Pierre Le
Goff. La Découverte. 1999
LQR. La propagande au quotidien. Eric Hazan. Raisons d’agir. 2006.
Pas de pitié pour les gueux. Sur les théories économiques du chômage. Laurent
Cordonnier. Liber. 2000
Les Econoclastes. Petit bréviaire des idées reçues en économie. La Découverte. 2003
Petit abécédaire de mots détournés. Nicole Malinconi. Editions Grand espace nord
(Belgique). 2008
Mots à maux. Dictionnaire de la lepénisation des esprits. Pierre Tévaian - Sylvie Tissot.
Editions Dagorno. 1991
1984. George Orwell et Amélie Audiberti. Gallimard. 1950.
Storytelling. La machine à fabriquer des histoires et à formater les esprits. Christian
Salmon. La Découverte. 2007
Petit lexique de la langue de bois. De quelques concepts et faux repères. Thérèse
Mercury. L’Harmattan. 2000
Les mots. Jean-Paul Sartre. Gallimard. Poche. 1ère édition Gallimard 1964.
Le dictionnaire des mots rares et précieux. Editions 10/18. 2005

## Informations sur cet ouvrage

Auteurs
Toute l'équipe de la SCOP Le pavé : http://www.scoplepave.org
Anthony BRAULT
Franck LEPAGE
Régis LEPRETRE
Annaïg MESNIL
Emmanuel MONFREUX
Alexia MORVAN
Sylvie PLANCKE
Gaël TANGUY