---
title: "The Interviewer"
date: 2020-10-25T08:00:00+01:00
tags: [Film, Court-Métrage]
draft: true
---

**Source** : [fr.wikipedia.org - Durak (Jeu de Carte)](https://fr.wikipedia.org/wiki/Dourak)    
**License** : [![](https://i.creativecommons.org/l/by-sa/3.0/88x31.png) Creative Commons Attribution -  Partage dans les Mêmes Conditions 3.0 International](http://creativecommons.org/licenses/by-sa/3.0/deed.fr)  
**Export** : 

---


<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://meta-tube.de/videos/embed/a2ebe515-3cc2-4220-a095-0817e3ff5102" frameborder="0" allowfullscreen></iframe>