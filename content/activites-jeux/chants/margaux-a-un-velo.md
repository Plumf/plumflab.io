---
title: "Margaux à Vélo"
date: 2020-10-28T08:00:00+01:00
tags: [Chants, Chanson à geste]
draft: true
---

**Source** : [fr.wikipedia.org - Durak (Jeu de Carte)](https://fr.wikipedia.org/wiki/Dourak)    
**License** : [![](https://i.creativecommons.org/l/by-sa/3.0/88x31.png) Creative Commons Attribution -  Partage dans les Mêmes Conditions 3.0 International](http://creativecommons.org/licenses/by-sa/3.0/deed.fr)  
**Export** : 

---

Margaux a un vélo 
Avec un trou dans son pneu
Qu'elle bouche avec son Chew Gum