---
title: "Durak"
date: 2020-10-28T08:00:00+01:00
tags: [Cartes, Jeux, Jeux Bistrot, 2 Joueur-seuses, 3 Joueur-seuses, 4 Joueur-seuses, 5 Joueur-seuses]
draft: true
---

**Source** : [fr.wikipedia.org - Durak (Jeu de Carte)](https://fr.wikipedia.org/wiki/Dourak)    
**License** : [![](https://i.creativecommons.org/l/by-sa/3.0/88x31.png) Creative Commons Attribution -  Partage dans les Mêmes Conditions 3.0 International](http://creativecommons.org/licenses/by-sa/3.0/deed.fr)  
**Export** : 

---

## But du jeu

Le but de chaque joueur est de se débarrasser le plus rapidement possible de la totalité de ses cartes, comme dans le jeu du Pouilleux ou du Mistigri.

## Ordre des cartes

L'ordre des cartes est le suivant, de la plus faible à la plus forte : 6, 7, 8, 9, 10, Valet, Dame, Roi, As. N'importe quelle carte d'atout est plus forte que toutes les cartes des autres couleurs.

## Déroulement d'une partie

Pour jouer, il faut posséder trente-six cartes (du 6 à l'as) et le nombre de joueurs peut être de deux à quatre.

Le dernier Dourak distribue six cartes à chaque joueur deux par deux ; s'il n'y a pas de Dourak, un joueur doit se sacrifier. Celui qui commence est celui qui a le plus petit atout (lorsqu'on commence une partie) ou le Maître Dourak de la partie précédente (celui qui a fini en premier).

Le Dourak distribue 6 cartes à chaque joueur, les cartes en trop formant la pioche. La première carte de la pioche est retournée et est placée sous celle-ci, sa couleur indique la couleur d'atout, elle sera la dernière carte piochée. Si on joue à 6, il n'y a pas de pioche, l'atout étant désignée par la couleur de la dernière carte du Dourak (qu'il doit alors montrer aux autres joueurs).

La partie se déroule de la façon suivante :

    Le premier attaquant est le joueur situé à la droite du Dourak, le Dourak étant le premier défenseur. En effet, il faut apprendre à jouer au Dourak puisqu'il est idiot.
    Si l'attaque échoue (voir ci-dessous), le défenseur devient attaquant et défie à son tour le joueur situé à sa gauche. Si l'attaque réussi, le défenseur passe son tour et c'est le joueur à sa gauche qui devient alors attaquant, il défie le joueur placé directement à sa gauche.
    Pour gagner, le défenseur, donc au début le Dourak, doit donner une carte plus forte que celle posée ou un atout. L'attaquant et les autres joueurs (sauf si le jeu se joue par équipe) peuvent poser des cartes de même valeur que les cartes déjà posées. Le but de l'attaquant est de faire perdre le défenseur en lui faisant prendre les cartes posées.
    Quand le défenseur n'a plus de cartes, il ne peut plus jouer ou encore quand les attaquants ne peuvent plus mettre de cartes, le tour s'arrête.
    Le premier attaquant prend des cartes dans la pioche jusqu'à en avoir six, puis chacun des attaquants fait de même dans le sens des aiguilles d'une montre et enfin le défenseur fait de même.
    le Dourak est le joueur à qui il reste des cartes en fin de parties. Si le Dourak perd sur une attaque de quatre 6 alors on dit qu'il "a pris ses galons".

## Modèle pour deux joueurs

#### Donne

On distribue six cartes par joueur, la dernière carte du paquet restant est retournée sur le tas donnant la couleur forte, celle qui vaut plus que toutes les autres. Celui qui commence est l’attaquant. Il y a deux phases de jeux, l’attaque et la défense.

### Premier tour

Celui qui attaque doit poser une ou plusieurs cartes (de même hauteur, ex : deux valets) et le défenseur doit contrer ces cartes avec des cartes plus fortes.

Si le défenseur ne peut pas (ou ne veut pas, par stratégie) contrer les cartes, il les ramasse, et les met dans son jeu.

### Deuxième tour

Au deuxième tour, l’attaquant doit jouer la valeur d’une carte qui a déjà été posée (celles qui sont sur la table ; ex : si l’attaquant pose un 9 et le défenseur une dame, l’attaquant doit poser soit une dame soit un 9). S’il ne peut pas jouer ou si le défenseur contre chacune des six attaques, il devient attaquant et l’attaquant devient défenseur.

À chaque fin de tour (de chaque pli) les joueurs complètent leurs jeux pour avoir six cartes (l’attaquant se sert en premier).

## Modèle pour quatre joueurs

Même règles que pour deux sauf que les joueurs jouent en équipes de deux : une équipe attaquante et une équipe défensive. Les attaquants peuvent s’entre-aider en posant une carte si celui qui attaque bloque le pli (n’a pas la bonne carte), et le rôle de l’attaquant tourne dans l’équipe. Lorsqu’un joueur n’a plus de cartes la partie reprend suivant le modèle pour trois joueurs.

## Modèle pour trois joueurs

Lors du jeu à trois, il y a un défenseur et deux attaquants. Même système que pour le jeu à deux, un joueur attaque son voisin de gauche, qui lui, défend, et le troisième joueur peut aider l’attaquant comme s'il était dans son équipe (comme dans la partie à quatre). Le défenseur devient attaquant s’il repousse l’attaque (6 plis) mais s’il perd on lui saute son tour d’attaque.
Variante

Dans la grande famille Ouss, les 52 cartes sont utilisées pour un maximum de rire. De plus, le dernier joueur qui recouvre a la fin de la partie peut tout de même ne pas devenir dourak en se recouvrant soi-même sans laisser des cartes seules, dans ce cas personne ne perd, ce qui est vraiment chouette!

## Fin de partie

La partie s’arrête quand aucun des joueurs n’a de cartes sauf un. Celui-ci est appelé « Dourak » (idiot) et il distribue à la prochaine partie. 