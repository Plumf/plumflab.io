---
title: "Le Tarot de Marseille"
date: 2020-10-28T08:00:00+01:00
tags: [Cartes, Jeux, Jeux Bistrot, 3 Joueur-seuses, 4 Joueur-seuses, 5 Joueur-seuses]
draft: true
---

**Source** : [www.letarot.net - Règle du Tarot](https://www.letarot.net/fr/rules.html)    
**License** : © - Copyright   
**Export** : 

---

> Les aides présentées ici sont une compilation du règlement de la Fédération Française de Tarot (1bis, rue des Cornillons - 71100 Chalon-sur-Saone).
>
>Pour débuter ou progresser au Tarot, je vous recommande “Le Jeu de Tarot” de Noël Chavey (Éditions Solar) qui m’a beaucoup aidé pour le développement du moteur de réflexion de mon logiciel LeTarot.

## Composition du Jeu

Le jeu de Tarot comprend 78 cartes:
 - 4 couleurs de 14 cartes chacune avec d’ordre décroissant Roi - Dame - Cavalier (honneur particulier au Tarot) - Valet puis 10 - 9 - 8 - 7 - 6 - 5 - 4 - 3 - 2 - As.
 - 21 Atouts (ou Tarots). Le numéro indique la force de chaque Atout: 21 le plus fort, 1 (ou Petit) le plus faible.
 - l’Excuse est la carte marquée d’une étoile et représentant un joueur de mandoline. Cette carte est une sorte de Joker dispensant de jouer la couleur ou l’Atout demandés, mais ne pouvant pas obtenir le gain du pli. L’Excuse est imprenable mais ne doit pas être jouée au dernier pli, dans ce cas elle change automatiquement de camp (exception voir Chelem).

Les 3 cartes les plus importantes sont les Oudlers (ou Bouts): le 21, le Petit et l’Excuse. Le 21 est bien sûr imprenable; le Petit est par contre vulnérable.

## Valeur des Cartes

Chacune des 78 cartes a une valeur déterminée de la façon suivante (pour éviter des demis points les cartes sont comptées 2 par 2)

| Cartes                        | Valeur   |
|-------------------------------|----------|
| Un Bout + une carte basse     | 5 points |
| Un Roi + une carte basse      | 5 points |
| Une Dame + une carte basse    | 4 points |
| Un Cavalier + une carte basse | 3 points |
| Un Valet + une carte basse    | 2 points |
| Deux cartes basses            | 1 point  |

**Total dans le jeu : 91 points**
 
## But du Jeu

Le Tarot est à la fois un jeu individuel et d’équipe. En effet, au cours d’une donne, l’un des joueurs (le Preneur) est opposé aux 3 autres (les Défenseurs) qui forment une équipe. Mais celle-ci ne dure que le temps d’une donne.  
Le Preneur s’engage au cours des enchères à réaliser un contrat, c’est à dire à atteindre grâce à ses levées, un certain nombre de points. Ce nombre minimum est déterminé exclusivement par les Bouts qu’il aura dans ses plis à la fin de la partie:

| Nombre de Bouts | Minimum de points |
|-----------------|-------------------|
|       0         |       56          |
|       1         |       51          |
|       2         |       41          |
|       3         |       36          |

## Désignation du Donneur

Avant la première distribution, le jeu est étalé face cachée, et chaque joueur tire une carte au hasard.  
La plus petite carte désigne le **Donneur**. En cas d’égalité, l’ordre de priorité entre les couleurs est appliqué : Pique-Cœur-Carreau-Trèfle. Ainsi l’As de Trèfle désigne automatiquement le **Donneur**. Le joueur ayant tiré l’Excuse doit retirer.

Pour les distributions suivantes, c’est le joueur placé à droite du précédent **Donneur** qui devient à son tour **Donneur**.

## Coupe et Distribution

Le jeu est battu par le joueur situé en face du **Donneur**. La coupe doit être effectuée par le joueur placé à gauche du **Donneur**. La plus petite partie du paquet doit comporter au moins 4 cartes.

Le Donneur distribue les cartes 3 par 3 dans le sens inverse des aiguilles d’une montre. Le Chien (6 cartes) est constitué au gré du Donneur mais une carte à la fois et sans la première ni la dernière du paquet.

Si un joueur détient le Petit comme seul atout dans sa main et sans posséder l’Excuse (Petit Sec), il doit le signaler immédiatement et la donne est annulée.

## Enchères

Après avoir pris connaissance de leur jeu, chaque joueur, en commençant par le joueur situé à droite du Donneur fait son annonce. Chaque joueur ne peut parler qu’une fois, il peut soit passer soit faire une enchère, soit une surenchère:
 - Passe. Si tous les joueurs ont passé, le voisin à droite du Donneur effectue une nouvelle donne.
 - Prise (ou Petite).
 - Garde. Le déroulement de ces 2 enchères est semblable, seul le score diffère… Le Chien est montré aux joueurs, puis le Preneur l’inclut dans son jeu pour enfin en écarter 6 cartes qui compteront dans ses plis.
 - Garde Sans le Chien. Le Chien face cachée est inclus aux plis du Preneur.
 - Garde Contre le Chien. Le Chien face cachée est inclus aux plis de la Défense.

## Ecart

Le Preneur d’une Prise ou d’une Garde mêle à son jeu les 6 cartes du Chien. Il écarte alors 6 cartes parmi les 24 qu’il détient. Cet Ecart reste secret et sera compté avec les plis du Preneur.

Ne peuvent être écartés ni Bouts ni Rois. De plus, le Preneur peut mettre des Atouts dans l’Ecart seulement si sa main ne contient que des Bouts, des Rois et des Atouts… Dans ce cas, les Atouts écartés doivent être montrés à la Défense.

 ## Jeu de la Carte

L’entame est effectuée par le joueur placé à droite du donneur. L’ordre des joueurs est dans le sens inverse des aiguilles d’une montre. Le joueur ayant obtenu le gain du pli entame le suivant. Le jeu se déroule suivant les règles suivantes:
 - Atout. On est obligé de monter sur l’Atout le plus fort déjà en jeu (surcouper). Au cas où le joueur ne peut pas surcouper, il joue n’importe quel Atout (sous-couper ou pisser).
 - Couleur. On est obligé de fournir la couleur demandée mais pas de monter.
 - Si on ne possède pas de carte de la couleur demandée, couper est obligatoire. Si un joueur a déjà coupé, il faut alors surcouper ou sinon sous-couper.
 - Si on n’a ni carte dans la couleur demandée, ni Atout, on joue une carte de son choix (Défausse).
 - Si l’entame est l’Excuse, c’est la 2ème carte qui détermine la couleur demandée.

## Primes

• le Petit au Bout. Dans le cas où le Petit fait partie de la dernière levée, le camp réalisant ce pli bénéficie d’une prime de 10 points, multipliable suivant le contrat quelque soit le résultat de la donne.

• la Poignée. Le joueur possédant une Poignée (10, 13 ou 15 Atouts) peut s’il le désire l’annoncer et l’exposer avant de jouer sa 1ère carte.
Simple Poignée (10 Atouts): prime de 20 points.
Double Poignée (13 Atouts): prime de 30 points.
Triple Poignée (15 Atouts): prime de 40 points.
Ces primes ne sont pas multipliables (voir Score) et sont acquises au camp vainqueur de la donne. L’excuse dans une poignée implique que le joueur n’a pas d’autre Atout.

## Chelem

Le Chelem consiste à remporter les 18 levées de la donne...  
Le Chelem peut être demandé en plus du contrat; une prime supplémentaire non multipliable est ajoutée au résultat du contrat :
• Chelem annoncé et réalisé: prime de 400 points.
• Chelem non annoncé mais réalisé: prime de 200 points.
• Chelem annoncé mais non réalisé: amende de 200 points.

L’annonce peut être faite après l’Ecart; l’annonceur d’un Chelem bénéficie alors de l’entame. Si le joueur tentant le Chelem possède l’Excuse, celle-ci peut être jouée en carte maîtresse au dernier pli si tous les autres plis ont été acquis; dans ce cas, le Petit sera considéré comme étant au bout à l’avant dernier pli.

 
## Score

Pour gagner son contrat, le Preneur doit réaliser un nombre de points fonction du nombre de Bouts qu’il possède (voir But du Jeu ).
Si le compte des points contenus dans les levées du Preneur est inférieur à son contrat, le Preneur chute celui-ci. Tout contrat vaut 25 points auquels on ajoute les points de gain ou de perte. Le nombre obtenu est multiplié par 2 pour une Garde, par 4 pour une Garde Sans ou par 6 pour une Garde Contre. Ce nombre est soustrait (ou ajouté en cas de chute) à chacun des 3 Défenseurs, et ajouté 3 fois (ou soustrait 3 fois en cas de chute) au Preneur.

Exemple: le Preneur gagne une Garde avec 43 points, 2 Bouts et le Petit au Bout. Son gain est de 43-41=2 points; (25 + 2 + 10 pour le Petit au Bout) x 2 pour la Garde = 74 points; résultat -74 points pour les 3 Défenseurs et 74 x 3 = 222 points pour le Preneur.