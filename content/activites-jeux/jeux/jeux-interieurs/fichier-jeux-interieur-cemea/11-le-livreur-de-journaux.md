---
title: "Le livreur de journaux"
date: 2020-10-17T08:00:00+01:00
tags: [Jeux Intérieurs, Jeux Assis, Avec matériel]
draft: true
---

**Principe du jeu** 
> Les joueurs sont assis en cercle sur des chaises. L'un d’ eux, journal en main, ayant laissé sa chaise libre, va se placer au centre. Son but est de livrer le journal en le déposant sur une chaise libre avant qu'elle ne soit attribuée à quelqu’ un d'autre par le joueur placé à à gauche de celle-ci.

## Matériel

Un journal plié ou roulé de façon à lui assurer une certaine rigidité et faciliter ainsi  sa manipulation. 

## Le jeu commence 

Les joueurs sont assis en cercle sur des chaises et l’un d'entre eux reçoit le journal; il se trouve ainsi désigné pour être le livreur.

## Le jeu se déroule

Le livreur, journal en main, se lève en libérant sa chaise. Le joueur assis à gauche de cette chaise, pose alors sa main droite sur celle-ci en disant, par exemple : « C'est pour Jacques ». Jacques essaie de s'asseoir sur cette chaise en libérant la sienne qui devient disponible pour une nouvelle livraison du journal. Mais le joueur assis à la gauche de cette nouvelle « chaise  libre » peut poser sa main dessus en disant : « C'est pour Clara ». Clara essaie de s'y asseoir et libère donc elle aussi sa chaise sur laquelle son voisin de gauche pose la main droite en appelant un autre joueur, et ainsi de suite. Ces appels et ces déplacements successifs libèrent rapidement des chaises différentes et embrouillent tous les joueurs. Quand le journal a été déposé sur une chaise, avant que celle-ci ne soit occupée par quelqu'un appelé par le joueur « de gauche », le livreur de journaux peut s'y asseoir. Il y a alors changement de rôle : ce même joueur de gauche, qui n ‘a pas réussi à attribuer la chaise placée à sa droite immédiate, devient le nouveau livreur de journaux et se place debout, le journal en main au centre du cercle.  
Le nouveau signal de départ est donné par le joueur qui se trouve à la gauche de cette nouvelle chaise libre.

## Le jeu s'achève

La fin du jeu est laissée à l'appréciation du meneur ou du groupe.

## Autre manières de jouer


## Remarques pédagogiques

 - Ce jeu trouve tout son intérêt quand on connaît préalablement les prénoms. Dans l'autre manière de jouer, ce n'est plus indispensable. 
 - La grandeur du cercle doit être adaptée au nombre de joueurs afin de respecter un équilibre des difficultés entre le livreur de journaux et les autres joueurs.


 AU Puc .


 - Les joueurs, face tournée vers le centre, sont assis sur des chaises placées en cercle. Une chaise vide est située au centre, c'est celle du « provocateur » qui se tient debout à l'intérieur du cercle en ayant en main un journal “roulé. Il vient défier l'un. des _ joueurs assis en le tou-. : chant à l'aide du; jour-. - nal. Très rapidement, il va ensuite dépo- ser le journal. sur la chaise .… libre du centre du cercle, puis essaie de venir s'asseoir à la … place du joueur: provoqué. Celui-ci ‘tente de s'emparer du journal posé sur la chaise et de toucher le | provocateur avant qu'il _ne soit assis. S'il réussit à. le toucher, il condamne le provocateur |

à-rester dans son rôle. S'il ne réussit pas, il _va le: remplacer et devient le nouveau: pro- | vocateur. :

° Dans la précipitation du; jeu, il peut arri- ver que le provocateur fasse tomber le; jour- : nal de la chaise centrale, il doit le ramasser et le replacer. correctement avant de conti- nuer.