---
title: "Jeux pouvant se jouer à l'intérieur - CEMEA Bretagne"
date: 2020-10-17T08:00:00+01:00
tags: [Jeux Intérieurs, Jeux Assis, Avec matériel]
draft: true
---

Comment utiliser ce fascicule

Chaque page correspond à un jeu.
En haut se trouvent le titre, l'effectif et le matériel à prévoir éventuellement. Puis vient la description détaillée et, en bas de la page pour la plupart des jeux, nous avons ajouté une ou plusieurs remarques. I1 s'agit de conseils d'organisation, d'observations pédagogiques ou d'indications d'autres façons de jouer.

---------

Nous avons divisé l'ensemble des jeux en deux grandes rubriques fonctionnelles.  
Les jeux y sont classés en tenant compte deLTl'effectif proposé (de peu à beaucoup de joueurs).
LT
1 - Jeux ne demandant aucun matériel

Ils ne nécessitent aucune préparation préalable sauf celle, évidente, de bien relire les régles.
Ceci est d'ailleurs conseillé pour tous les types de jeux.
Il y a 45 jeux dans cette rubrique.

2 - Jeux demandant du matériel

En l'absence de ce matériel, ils ne peuvent se dérouler correctement.  
Le matériel peut se réduire à un foulard... ou une feuille de papier et un crayon. Mais il peut être plus important ; la préparation exige alors beaucoup de temps, par exemple : établir une liste d'actions simples à mimer qui tient compte de l'äge ou des possibilités des joueurs... ou trouver une grande quantité d'objets variés à observer pour s'en souvenir.  
Il y a 67 jeux dans cette rubrique.

Autre manière d'utiliser ce fascicule

Comme les jeux peuvent être utilisés avec une intention pédagogique précise, sans exclure l'amusement, nous avons mis en valeur les thèmes suivants (un même jeu peut se trouver dans plusieurs thèmes).  
Pour faciliter le choix, un tableau à double entrée se trouve à la fin du fascicule (pages 125 et 126) avec, d'une part, les 112 jeux classés par ordre alphabétique, d'autre part, les thèmes signalés ci-dessous.

Jeux où il faut parler

La parole est indispensable pour le bon déroulement du jeu :
 - soit un chiffre ou un simple mot  
 - soit une formule apprise ou la libre expression des joueurs