---
title: "Jeu N.A.S.A - Prise de décision"
date: 2020-10-28T08:00:00+01:00
tags: [Travail d'équipe, Communication, Exercice]
---

**Source** : [cpe.ac-dijon.fr - Jeu de rôle de prise de décision "la N.A.S.A"](http://cpe.ac-dijon.fr/spip.php?article22)    
**License** : [© www.ac-dijon.fr - droits réservés](http://cpe.ac-dijon.fr/spip.php?article22)  
**Export** : 

---

## Description brève

Imaginez un accident sur la lune : il s’agit de trouver l’équipement indispensable pour rejoindre la fusée mère, en classant 15 objets par ordre d’importance (voir annexe 1 : feuille d’instruction).
L’exercice se fait individuellement puis en groupe. On compare ensuite les Différents classements.
 - Nombre de participants : groupe de 6 à 8 personnes
 - Temps : environ 2 heures  et demi

## Objectifs

 1. Comparer l’efficacité de la prise de décision individuelle et la prise de décision collective.
 2. Montrer que le fonctionnement d’un groupe dépend directement des méthodes de travail de ses membres en comparant le mode de prise de décision par vote et le mode de décision consensuel.
 3. Montrer que les conflits, bien maîtrisés, favorisent la créativité.
 4. Apprendre à un groupe à ne pas sous-estimer son propre potentiel pour accroître son efficacité.

## Déroulement

L’exercice se déroule en 4 phases successives

### 1. Classement individuel (environ 10 minutes)

Chaque participant, après avoir reçu la feuille d’instructions (annexe 1), remplit un exemplaire de la feuille de décision (annexe 2) . Durant cette phase, Auncun échange entre les participants n’ est autorisé.

### 2. Classement collectif (45 à 60 minutes)

Les participants tiennent une réunion pour déterminer un classement collectif des mêmes éléments, en sous-groupes de 5 à 6 personnes.
Avec une dizaine de personnes, on peut structurer le groupe de la façon suivante : 7 participants, un animateur et 2 observateurs.
Lorsque les participants sont plus nombreux, on peut les répartir en sous-groupes qu’on laisse travailler librement ou bien auxquels on donne des consignes différentes de travail, en les invitant à prendre de façons différentes leurs décisions, par exemple, les uns à la majorité simple, d’autres en consensus : tous les participants doivent être d’accord, un seul d’entre eux peut bloquer le groupe s’il le juge nécessaire.
Il est possible, encore, de ne donner aucune consigne au 1er sous-groupe, de le laisser libre de s’organiser, mais de présenter au 2nd , avant l’exercice, quelques règles fondamentales de travail en groupe (annexe 3) permettant de mieux parvenir à l’unanimité.

### 3. Comparaison des classements

Quand les classements collectifs sont terminés, dans chaque  sous-groupe, l’animateur donne aux participants en s’appuyant sur l’annexe 5, le classement type fourni par la N.A.S.A. et leur demande de le transcrire, au fur et à mesure, dans les cases correspondantes, sur leur feuille de décision (annexe 2), 
Les participants calculent ensuite leurs points d’écart, c’est-à-dire, pour chaque rubrique, la différence, en valeur absolue, entre leur classement et celui de la N.A.S.A. La somme des différences constituera leur résultat individuel (annexe 2), 
IL procèdent de la même façon en comparant les résultats collectifs de chaque sous-groupe à ceux de la N.A.S.A. (annexe 2), 
Après avoir calculé la moyenne des résultats individuels en divisant la somme de ces derniers par le nombre des participants, on pourra comparer :
les résultats du meilleur,
les résultats moyens de chaque sous-groupe avant la discussion,
 les résultats de chaque sous-groupe après la discussion, avec décisions à la majorité simple ou consensus (annexe 4).

>S’il n’ apparaît pas opportun de demander des résultats individuels on pourra se contenter de demander les résultats  de chaque sous-groupe, après discussion. On observe alors, généralement, que, s’il existe des résultats individuels meilleurs que ceux du sous-groupe, il en est fait état d’une manière ou d’une autre.

### 4. Explication du classement fourni par la N.A.S.A.

L’animateur donne aux participants les critères sur lesquels se sont appuyés les experts de la N.A.S.A. pour établir leur classement. Les participants peuvent également indiquer également leurs raisons.

## Analyse de l'exercice

### 1. Fonctionnement du groupe

Le sous-groupe et l’ animateur se sont-ils fixé un plan ? (exemple : éléments vitaux, utiles, inutiles…).
Chaque participant a-t-il eu la possibilité de s’exprimer ?
Chaque participant écoutait-il les suggestions venant des autres ou cherchait-il à imposer sa propre liste ?
Y a-t-il eu des phénomènes de leadership, de conflit ou des regroupements à l’intérieur des sous-groupes ?
Combien de temps ont demandé les différentes décisions ?
Certains sous-groupes ont-ils adopté des modes de décision tels que la loi de la majorité, les concessions réciproques ou le hasard ? Ont-ils fait preuve de créativité ?

### 2. Décisions individuelles et collectives.

Quelles conclusions peut-on tirer de la comparaison des résultats individuels et collectifs (colonnes 1 et 3- Annexe 4).

> Dans la majorité des cas, le sous-groupe obtient des résultats meilleurs que les personnes qui le composent. Ceci est dû à l’élimination des erreurs par l’échange des connaissances entre participants et à la créativité de groupe qui trouve une façon originale d’utiliser certains éléments.

Quelles conclusions peut-on tirer de la comparaison du résultat collectif avec le ou les meilleurs résultats  individuels (colonnes 2 et 3- Annexe 4).

> Il arrive très fréquemment qu ‘une personne sans compétence technique particulière dans le domaine spatial obtienne des résultats meilleurs que le sous-groupe auquel elle appartient.
>
>En effet, l’utilisation des quinze objets pour atteindre les 2 objectifs essentiels, survivre et se déplacer, est moins fonction de connaissances techniques que la clarté avec laquelle ces 2 objectifs sont perçus et exprimés, et de l’invention dont chacun peut faire preuve pour utiliser de façon imprévue, dans un contexte nouveau, les objets à sa disposition.
>
>On observera que le résultat collectif est corrélativement meilleur lorsque le mode de prise de décision permet l’utilisation des toutes les ressources du groupe ; en ce sens, le mode de décision consensuel, par exemple, donne de meilleurs résultats dans l’ensemble que le mode de décision à la majorité.
>
> Avec l’aide des observateurs dans chaque sous-groupe, il sera possible d’affiner l’analyse et d’en tirer un certain nombre de réflexions sur les conditions favorables à l’efficacité d’un travail en groupe.

# ANNEXE 1 - Instructions

`(A remettre à chaque participant)`

>Vous faites partie de l’équipage d’ un vaisseau spatial programmé à l’ origine pour rejoindre une fusée mère de la face éclairée de la lune. A la suite d’ ennuis mécaniques, vous avez dû alunir à 320 kms environ du rendez-vous fixé. Au cours de l’alunissage, la plupart des équipements de bord ont été endommagés, à l’exclusion des 15 objets ci-dessous. Il est vital pour votre équipage de rejoindre la fusée mère et vous devez choisir l’équipement indispensable pour ce long voyage.
>L’ exercice consiste à classer les 15 objets par ordre de première nécessité. Mettez le chiffre 1 en face de celui qui vous semble le plus important, 2 en face du suivant, et ainsi de suite jusqu’à 15 en face de celui qui vous paraît le moins utile (Annexe 2- feuille de décision).

 - Une boîte d’allumette
 - Des aliments concentrés
 - 50 mètres de corde en nylon
 - Un parachute en soie
 - Un appareil de chauffage fonctionnant sur l’énergie solaire
 - 2 pistolets calibre 45
 - Une caisse de lait en poudre
 - 2 réservoirs de 50 kg d’oxygène chacun
 - Une carte céleste des constellations lunaires
 - Un canot de sauvetage auto-gonflable
 - Un compas magnétique
 - 25 litres d’eau
 - Une trousse médicale et des seringues hypodermiques
 - Des signaux lumineux
 - Un émetteur-récepteur fonctionnant sur l’énergie solaire (fréquence moyenne)

 # ANNEXE 2 - Feuille de décision

 | Objet | Classement individuel | Points d'écart | Classement collectif | Points d'écart | NASA |
 |-------|-----------------------|----------------|----------------------|----------------|-----|
| Une boîte d’allumette |  |  |  |  |  |
| Des aliments concentrés |  |  |  |  |  |
| 50 mètres de corde en nylon |  |  |  |  |  |
| Un parachute en soie |  |  |  |  |  |
| Un appareil de chauffage fonctionnant sur l’énergie solaire |  |  |  |  |  |
| 2 pistolets calibre 45 |  |  |  |  |  |
| Une caisse de lait en poudre |  |  |  |  |  |
| 2 réservoirs de 50 kg d’oxygène chacun |  |  |  |  |  |
| Une carte céleste des constellations lunaires |  |  |  |  |  |
| Un canot de sauvetage auto-gonflable |  |  |  |  |  |
| Un compas magnétique |  |  |  |  |  |
| 25 litres d’eau |  |  |  |  |  |
| Une trousse médicale et des seringues hypodermiques |  |  |  |  |  |
| Des signaux lumineux |  |  |  |  |  |
| Un émetteur-récepteur fonctionnant sur l’énergie solaire (fréquence moyenne) |  |  |  |  |  |
|  | Total : |  | Total : |  |  |

# ANNEXE 3 - Règles fondamentales du travail en groupe

 1. Eviter d’ imposer violemment son avis. Présenter son point de vue de façon aussi logique et lucide que possible, mais écouter attentivement les réactions des autres participants et les reformuler avant d’ insister sur sa propre position.
 2. Si la discussion semble engagée dans une impasse, éviter de privilégier les points de vue majoritaires et s’attacher à chercher la solution la plus acceptable pour tous les participants.
 3. Ne pas occulter une divergence sous le simple prétexte d ‘éviter un conflit et de sauvegarder un climat harmonieux. Se méfier des accords superficiels obtenus trop vite et trop facilement. Peser les arguments et s’ assurer que la décision set acceptée par tous pour des raisons fondamentalement similaires ou complémentaires. Ne soutenir que les positions qui reposent sur des bases à la fois logiques et objectives.
 4. Eviter les formules toutes faites de résolution des conflits, telles que la loi de la majorité, le hasard ou l’échange de concessions. Si un membre opposant finit par céder sur un point, ne pas considérer qu’il est indispensable de le récompenser en lui cédant sur un autre point.
 5. Les divergences d’ opinions sont normales et inévitables. Les mettre en évidence et s’ attacher à ce que tous participent au processus de décision. Dans la mesure où les divergences offrent au groupe un éventail très large d’ informations et opinions, elles représentent un gage de réussite pour la décision finale.

 # ANNEXE 4 - Comparaison des resultats

 |     | Moyenne des resultats (1) | Resultat le meilleur (2) | Resultat du groupe après la discussion |
 |---|---|---|---|
 | Sous-groupe 1 |  |  |  |
 | Sous-groupe 2 |  |  |  |
 | Sous-groupe 3 |  |  |  |
 | Sous-groupe 4 |  |  |  |

 # ANNEXE 5 - Resultats et explication fournis par la NASA

 Pour établir leur classement, les experts de la N.A.SA. se sont basés sur l’ utilisation alternée de 2 critères :
 - ce qui assure la vie biologique
 - ce qui assure la possibilité de rejoindre la fusée mère.

Ces 2 critères signifiant, par leur association, la survie.

| Objets | L’intitulé | Classement NASA |
|--------|------------|-----------------|
| Une boîte d’allumette | L’absence d’ oxygène ne permet pas de les enflammer | 15 |
| Des aliments concentrés | Moyen efficace de réparer les pertes d’énergie | 4 |
| 50 mètres de corde en nylon | Utiles pour se mettre en cordée, escalader les rochers ; éventuellement pour hisser les blessés | 6 |
| Un parachute en soie | Peut servir à se protéger des rayons solaires | 8 |
| Un appareil de chauffage fonctionnant sur l’énergie solaire | Sans utilité : les combinaisons sont chauffantes | 13 |
| 2 pistolets calibre 45 | Peuvent servir à accélérer la propulsion ; à la rigueur à mettre fin à ses jours | 11 |
| Une caisse de lait en poudre | Piège nutritionnel : plus encombrant que les aliments concentrés | 12 |
| 2 réservoirs de 50 kg d’oxygène chacun | Premier élément de survie essentiel | 1 |
| Une carte céleste des constellations lunaires | Indispensable pour s’orienter | 3 |
| Un canot de sauvetage auto-gonflable | Peut servir de traîneau pour tracter des objets ; le gaz(CO) employé pour cet engin peut servir à la propulsion | 9 |
| Un compas magnétique | Sans utilité sur la lune ; le champ magnétique n’y étant pas valorisé | 14 |
| 25 litres d’eau | Indispensable pour compenser une forte déshydratation due à la très grande chaleur sur la face éclairée de la lune | 2 |
| Une trousse médicale et des seringues hypodermiques | Les piqûres de vitamines, sérum etc…nécessitent une ouverture spéciale (prévue par la N.A.S.A.) | 7 |
| Des signaux lumineux | Utiles quand la fusée mère sera en vue | 10 |
| Un émetteur-récepteur fonctionnant sur l’énergie solaire (fréquence moyenne) | Utiles pour essayer de communiquer avec la fusée mère mais cet appareil n’a pas grande portée | 5 |

